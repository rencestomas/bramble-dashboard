import { useEffect } from "react";
import OrderItem from "../../app/components/OrderItem/OrderItem";
import useAxiosSecure from "../../hooks/useAxiosSecure";
import StackGrid from "react-stack-grid";
import { RootState } from "../../app/core/store/store";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { addOrders } from "../../app/core/store/features/order/orderSlice";
import { OrderStateEnum } from "../../app/shared/interfaces/order-state.interface";
import { Order } from "../../app/shared/interfaces/order.interface";
import { useTranslation } from "react-multi-lang";


export default function Orders() {

    const axiosSecure = useAxiosSecure();
    const auth = useSelector((state: RootState) => state.auth);
    const navigate = useNavigate();

    const orders = useSelector((state: RootState) => [...state.order.orders])
        .filter(c => c.restaurationId && c.restaurationId === auth.user?.restaurationId);

    const dispatch = useDispatch();
    const t = useTranslation();


    useEffect(() => {
        async function apiCall() {
            let response = await axiosSecure.get(`/api/order?restaurationId=${auth?.user?.restaurationId}`, true);
            
            dispatch(addOrders(response.data?.map((order: Order) => ({
                ...order,
                state: order?.orderStateId === OrderStateEnum.IsDone ? t('OrderIsDone') : (
                    order?.orderStateId === OrderStateEnum.IsPreparing ? t('OrderIsPreparing') : (order?.orderStateId === OrderStateEnum.IsCanceled ? t('OrderIsCanceled') : t('OrderIsAccepted'))
                )
            }))));
        }

        apiCall();
    }, [])

    useEffect(() => {
        let isOpen = auth?.restauration?.isOpen;

        if (!isOpen) {
            navigate("/overview");
        }
    })

    return (
        <StackGrid
            columnWidth={270}
        >
            {orders.map((order, index) => (
                <OrderItem
                    key={index}
                    order={order}
                />
            ))}
        </StackGrid>
    );
}