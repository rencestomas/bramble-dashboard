import Box from '@mui/material/Box';
import useAxiosSecure from '../../hooks/useAxiosSecure';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { RootState } from '../../app/core/store/store';
import { Category } from '../../app/shared/interfaces/category.interface';
import { Button, Stack, useTheme } from '@mui/material';
import dayjs from 'dayjs';
import { capitalize, getDayName, newGuid } from '../../app/core/utilities';
import CategoryList from '../../app/components/CategoryList/CategoryList';
import DatePicker from '../../app/components/DatePicker/DatePicker';
import { useTranslation } from 'react-multi-lang';
import { addCategories } from '../../app/core/store/features/category/categorySlice';


export default function DailyMenu() {

    const [dailyDraftCategories, setDailyDraftCategories] = useState<any[]>([]);
    const [week, setWeek] = useState<string[]>([]);

    const auth = useSelector((state: RootState) => state.auth);

    const dailyCategories = useSelector((state: RootState) => [...state.category.categories])
        .filter(c => !c.isStandingOffer
            && c.restaurationId === auth.user?.restaurationId
            && c.currentForDay
            && new Date(c.currentForDay) <= new Date(week[week.length - 1])
            && new Date(c.currentForDay) >= new Date(week[0])
        )
        .sort((a: Category, b: Category) => a.viewOrder - b.viewOrder);

    //   const standingOfferCategories = useSelector((state: RootState) => [...state.category.categories])
    //     .filter(c => c.isStandingOffer && c.restaurationId === auth.user?.restaurationId)
    //     .sort((a: Category, b: Category) => a.viewOrder - b.viewOrder);


    const axiosSecure = useAxiosSecure();
    const dispatch = useDispatch();
    const t = useTranslation();
    const theme = useTheme();


    useEffect(() => {
        async function apiCall() {

            let filter = "";

            if (auth.user) {
                filter += `restaurationId=${auth.user?.restaurationId}&`;
            }

            let response = await axiosSecure.get(`/api/category?${filter}`, true);

            dispatch(addCategories(response.data));
        }

        apiCall();
    }, [])

    async function handleCreateWeek() {

        let response = await axiosSecure.post(`/api/category/many`,
            dailyDraftCategories, true);

        if (response.data && response.data.length > 0) {
            dispatch(addCategories(response.data?.map((c: any) => ({
                ...c,
                _isEditing: false,
                _isDraft: false
            }))));
        }
    }

    async function loadDailyCategories(from: string, to: string) {

        let filter = "";

        if (auth.user) {
            filter += `restaurationId=${auth.user?.restaurationId}&`;
        }

        filter += `currentForDayFrom=${from}&`;
        filter += `currentForDayTo=${to}`;


        let response = await axiosSecure.get(`/api/category?${filter}`, true);

        dispatch(addCategories(response.data?.map(
            (c: any) => ({
                ...c,
                _isEditing: false
            })
        )));
    }

    async function handleOnDateChange(event: any[]) {

        await loadDailyCategories(event[0], event[event.length - 1])

        setDailyDraftCategories(event.map((date, index) => ({
            id: newGuid(),
            name: `${capitalize(getDayName(date, 'sk-SK'))} ${dayjs(date).format("DD.MM.YYYY")}`,
            restaurationId: auth.user?.restaurationId,
            viewOrder: index,
            isStandingOffer: false,
            currentForDay: date,
            menuitems: [],
            _isEditing: false,
            _isDraft: true
        })));

        setWeek(event);
    };

    return (
        <>
            <DatePicker
                onChange={handleOnDateChange}
            />

            <Stack
                direction="row"
                alignItems="space"
                spacing={2}
            >
                {/* <WeekPicker
                    onChange={handleOnDateChange}
                /> */}

                <Box
                    sx={{
                        width: '100%'
                    }}>

                    <Button
                        color="primary"
                        variant="contained"
                        onClick={handleCreateWeek}
                        sx={{
                            marginBottom: '10px',
                            visibility: (dailyCategories?.length > 0) ? 'hidden' : 'visible'
                        }}
                    >
                        {t('CreateWeek')}
                    </Button>


                    <CategoryList
                        categories={dailyCategories?.length > 0 ? dailyCategories : dailyDraftCategories}
                    />
                </Box>
            </Stack>
        </>
    );
}