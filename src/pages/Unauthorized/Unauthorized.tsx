import { Box, Typography, useTheme } from "@mui/material";
import { useTranslation } from "react-multi-lang";

export default function Unauthorized() {

    const theme = useTheme();
    const t = useTranslation();

    return (

        <Box
            sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                minHeight: "100vh"
            }}
        >
            <Typography
                sx={{
                    // fontSize: "xxx-large",
                    fontSize: {
                        lg: "xxx-large",
                        md: "xx-large",
                        sm: "x-large"
                    },
                    fontWeight: "bold",
                    color: theme.palette.text.primary
                }}
            >
                {t('NoPermission')}
            </Typography>
        </Box>
    );
}