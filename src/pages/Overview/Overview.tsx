import { Box, Button, Stack, Typography, useTheme } from "@mui/material";
import { useEffect } from "react";
import { useTranslation } from "react-multi-lang";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { updateUserRestauration } from "../../app/core/store/features/auth/authSlice";
import { RootState } from "../../app/core/store/store";
import useAxiosSecure from "../../hooks/useAxiosSecure";

export default function Overview() {

    const theme = useTheme();
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const axiosSecure = useAxiosSecure();
    const auth = useSelector((state: RootState) => state.auth);
    const t = useTranslation();
    

    useEffect(() => {
        let isOpen = auth?.restauration?.isOpen;

        if (isOpen) {
            navigate("/overview/orders");
        }
    })


    async function handleOpenRestaurant() {
        let _restauration = ({ ...auth?.user?.restauration });
        _restauration!.isOpen = true;

        let response = await axiosSecure.put(`/api/restauration`,
            {
                id: _restauration.id,
                isOpen: true
            }, true);

        dispatch(updateUserRestauration({
            restauration: _restauration as any
        }));

        navigate("/overview/orders");
    }

    return (
        <Box
            sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                minHeight: "100vh"
            }}
        >
            <Stack spacing={2} alignItems="center">
                <Typography paragraph
                    sx={{
                        // fontSize: "xxx-large",
                        fontSize: {
                            lg: "xxx-large",
                            md: "xx-large",
                            sm: "x-large"
                        },
                        fontWeight: "bold",
                        color: theme.palette.text.primary
                    }}
                >
                    {t('OrdersViewLabel')}
                </Typography>
                <Button size="large" variant="contained"
                    onClick={handleOpenRestaurant}
                    sx={{
                        fontSize: {
                            lg: "large",
                            md: "medium",
                            sm: "small"
                        },
                        padding: {
                            lg: "20px",
                            md: "15px",
                            sm: "10px"
                        },
                        // color: theme.palette.text.primary
                    }}
                >
                    {t('OpenRestauration')}
                </Button>
            </Stack>
        </Box>
    );
}