
import useAxiosSecure from '../../hooks/useAxiosSecure';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { RootState } from '../../app/core/store/store';
import { Category } from '../../app/shared/interfaces/category.interface';
import { useTheme } from '@mui/material';
import CategoryList from '../../app/components/CategoryList/CategoryList';
import { addCategories } from '../../app/core/store/features/category/categorySlice';


export default function StandingOffer() {

    const auth = useSelector((state: RootState) => state.auth);

    const standingOfferCategories = useSelector((state: RootState) => [...state.category.categories])
        .filter(c => c.isStandingOffer && c.restaurationId === auth.user?.restaurationId)
        .sort((a: Category, b: Category) => a.viewOrder - b.viewOrder);


    const axiosSecure = useAxiosSecure();
    const dispatch = useDispatch();
    const theme = useTheme();


    useEffect(() => {
        async function apiCall() {

            let filter = "";

            if (auth.user) {
                filter += `restaurationId=${auth.user?.restaurationId}&`;
            }

            let response = await axiosSecure.get(`/api/category?${filter}`, true);

            dispatch(addCategories(response.data));
        }

        apiCall();
    }, [])


    return (
        <>
            <CategoryList
                categories={standingOfferCategories}
            />
        </>
    );
}