import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import StackGrid from "react-stack-grid";
import { RootState } from "../../app/core/store/store";
import { useNavigate } from "react-router-dom";
import useAxiosSecure from "../../hooks/useAxiosSecure";
import { addRestaurants } from "../../app/core/store/features/restaurant/restaurantSlice";
import { addFestivals } from "../../app/core/store/features/festival/festivalSlice";
import RestaurantItem from "../../app/components/RestaurantItem/RestaurantItem";
import FestivalItem from "../../app/components/FestivalItem/FestivalItem";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import AddIcon from '@mui/icons-material/Add';
import { Accordion, AccordionSummary, Button, IconButton, Stack, Typography, styled, useTheme } from "@mui/material";
import { setPanelContext } from "../../app/core/store/features/sidePanel/sidePanelSlice";
import { newGuid } from "../../app/core/utilities";
import { useTranslation } from "react-multi-lang";


const RoundedAccordion = styled(Accordion)(() => ({
    marginBottom: '0.7rem',
    borderRadius: '0.6rem !important',
    '&:before': {
        display: 'none',
    },
    width: '100%'
}));

const RoundedAccordionSummary = styled(AccordionSummary)(({ theme }) => ({
    color: 'black',
    borderRadius: '8px',
    '&.Mui-expanded': {
        background: theme.palette.primary.main,
        color: 'white'
    },
    '.MuiAccordionSummary-expandIconWrapper': {
        color: 'black !important'
    },
    '.MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
        color: 'white'
    }
}));

export default function Admin() {
    const axiosSecure = useAxiosSecure();
    const theme = useTheme();
    const auth = useSelector((state: RootState) => state.auth);
    const navigate = useNavigate();

    const restaurants = useSelector((state: RootState) => [...state.restaurant.restaurants]);
    const festivals = useSelector((state: RootState) => [...state.festival.festivals]);

    const t = useTranslation();
    const dispatch = useDispatch();


    const iconStyle = {
        cursor: 'pointer',
        color: 'white'
    }

    useEffect(() => {
        async function apiCall() {
            let response = await axiosSecure.get(`/api/restauration`, true);
            dispatch(addRestaurants(response.data));
        }

        apiCall();
    }, [])


    useEffect(() => {
        async function apiCall() {
            let response = await axiosSecure.get(`/api/festival`, true);
            dispatch(addFestivals(response.data));
        }

        apiCall();
    }, [])

    function handleCreateFestivalClick(event: any) {
        event.stopPropagation();
        dispatch(setPanelContext({
            action: 'CREATE',
            contextType: 'Festival',
            contextObject: {
                id: newGuid()
            }
        }));
    }

    function handleCreateRestaurantClick(event: any) {
        event.stopPropagation();
        dispatch(setPanelContext({
            action: 'CREATE',
            contextType: 'Restaurant',
            contextObject: {
                id: newGuid()
            }
        }));
    }

    return (
        <>
            <RoundedAccordion
                disableGutters
                elevation={0}
                defaultExpanded={true}
            >
                <RoundedAccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    sx={{
                        color: 'black',
                        borderRadius: '8px',
                        '&.Mui-expanded': {
                            background: theme.palette.primary.main,
                            color: 'white'
                        },
                        '.MuiAccordionSummary-expandIconWrapper': {
                            color: 'black !important'
                        },
                        '.MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
                            color: 'white !important'
                        }
                    }}
                >
                    <Stack
                        spacing={2}
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                        sx={{
                            width: '100%',
                            height: '20px'
                        }}
                    >

                        <Typography>
                            {t('Festivals')}
                        </Typography>

                        <IconButton
                            color="black"
                            size='small'
                            onClick={handleCreateFestivalClick}
                        >
                            <AddIcon sx={iconStyle} />
                        </IconButton >

                    </Stack>

                </RoundedAccordionSummary>

                <StackGrid
                    columnWidth={270}
                >
                    {festivals.map((festival, index) => (
                        <FestivalItem
                            key={index}
                            festival={festival}
                        />
                    ))}
                </StackGrid>

            </RoundedAccordion >


            <RoundedAccordion
                disableGutters
                elevation={0}
                defaultExpanded={true}
            >
                <RoundedAccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    sx={{
                        color: 'black',
                        borderRadius: '8px',
                        '&.Mui-expanded': {
                            background: theme.palette.primary.main,
                            color: 'white'
                        },
                        '.MuiAccordionSummary-expandIconWrapper': {
                            color: 'black !important'
                        },
                        '.MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
                            color: 'white !important'
                        }
                    }}
                >
                    <Stack
                        spacing={2}
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                        sx={{
                            width: '100%',
                            height: '20px'
                        }}
                    >

                        <Typography>
                            {t('Restaurants')}
                        </Typography>

                        <IconButton
                            color="black"
                            size='small'
                            onClick={handleCreateRestaurantClick}
                        >
                            <AddIcon sx={iconStyle} />
                        </IconButton >

                    </Stack>

                </RoundedAccordionSummary>

                <StackGrid
                    columnWidth={270}
                >
                    {
                        restaurants.map((restaurant, index) => (
                            <RestaurantItem
                                key={index}
                                restaurant={restaurant}
                            />
                        ))
                    }
                </StackGrid>

            </RoundedAccordion >

        </>
    );
}