import { RootState } from '../app/core/store/store';
import { useSelector } from 'react-redux';


const useLoading = () => {
    const loading = useSelector((state: RootState) => state?.loading);
    return loading;
}

export default useLoading;