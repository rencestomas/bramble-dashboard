import { apiSecure } from "../app/core/api/axios";
import { useEffect } from "react";
import useRefreshToken from "./useRefreshToken";
import { RootState } from "../app/core/store/store";
import { useDispatch, useSelector } from 'react-redux';
import { ExceptionMap } from "../app/core/api/exceptionsMapping";
import { showLoading } from "../app/core/store/features/loading/loadingSlice";
import { displayNotification } from "../app/core/store/features/notification/notificationSlice";


const useAxiosSecure = () => {
    const dispatch = useDispatch();
    const refresh = useRefreshToken();
    const auth = useSelector((state: RootState) => state.auth)

    useEffect(() => {

        // const requestIntercept = apiSecure.interceptors.request.use(
        //     config => {
        //         if (config?.headers && !config.headers['Authorization']) {
        //             config.headers['Authorization'] = `Bearer ${auth?.accessToken}`;
        //         }
        //         return config;
        //     }, (error) => Promise.reject(error)
        // );

        const responseIntercept = apiSecure.interceptors.response.use(
            response => {
                return response;
            },
            async (error) => {
                const prevRequest = error?.config;
                // if response status is 401 - unauthorized, try refresh token and send request again
                if (error?.response?.status === 401 && !prevRequest?.sent) {
                    prevRequest.sent = true;
                    // const newAccessToken = await refresh();
                    await refresh();
                    // prevRequest.headers['Authorization'] = `Bearer ${newAccessToken}`;
                    return apiSecure.request(prevRequest);
                }
                return Promise.reject(error);
            }
        );

        const loadingResponseIntercept = apiSecure.interceptors.response.use(
            response => {
                return response;
            },
            async (error) => {

                dispatch(showLoading({
                    showLoading: false
                }));

                return Promise.reject(error);
            }
        );

        const errorIntercept = apiSecure.interceptors.response.use(
            response => {
                return response;
            },
            async (error) => {

                let message;

                if (!error.response) {
                    message = "Chyba pri pripájaní k serveru. Skontrolujte svoje internetové pripojenie.";
                } else {
                    message = ExceptionMap[error.response.data.detail.errorCode] || "Neznáma chyba.";
                }

                dispatch(displayNotification({
                    message: message,
                    severity: "error"
                }));

                return Promise.reject(error);
            }
        );

        return () => {
            // apiSecure.interceptors.request.eject(requestIntercept);
            apiSecure.interceptors.response.eject(responseIntercept);
            apiSecure.interceptors.response.eject(loadingResponseIntercept);
            apiSecure.interceptors.response.eject(errorIntercept);
        }
    }, [auth, refresh])

    return apiSecure;
}

export default useAxiosSecure;
