export interface Order {
    id: string;
    note: string;
    userEmail: string;
    orderNumber: string;
    tableId: string;
    table: any;
    orderStateId: string;
    orderState: any;
    orderStateReason: string;
    restaurationId: string;
    restauration: any;
    restaurantName: string;
    price: number;
    orderMenuitems: any[];
    createdAt: string;
    updatedAt: string;
}