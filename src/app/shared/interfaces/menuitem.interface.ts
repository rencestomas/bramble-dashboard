import { ExtraCategory } from "./extra-category.interface";
import { MenuItemExtraCategory } from "./menuitem-extra-category.interface";

export interface Menuitem {
    id: string;
    categoryId: string;
    name: string;
    description: string;
    weight: string;
    allergens: string;
    price: number;
    viewOrder: number;
    isHidden: boolean;
    diet: number;
    menuitemExtraCategories: MenuItemExtraCategory[];

    _isEditing: boolean;
}