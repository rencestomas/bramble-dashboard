export interface SecurityRole {
    id: string;
    name: string;
}
