import { ExtraCategoryExtraMenuitem } from "./extra-category-extra-menuitem.interface";
import { MenuItemExtraCategory } from "./menuitem-extra-category.interface";

export interface ExtraCategory {
    id: string;
    name: string;
    restaurationId: string;
    viewOrder: number;
    isOneChoice: boolean;
    isMandatory: boolean;

    menuItemExtraCategories: MenuItemExtraCategory[];
    extraCategoryExtraMenuitems: ExtraCategoryExtraMenuitem[];

    // _isEditing: boolean;
    _isDraft: boolean;
}