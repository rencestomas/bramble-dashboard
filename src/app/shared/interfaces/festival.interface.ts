
export interface Festival {
    id: string;
    name: string;
    description: string;
    siteUrl: string;
    street: string;
    zipCode: string;
    city: string;
    state: string;
    
    festivalRestaurants: [];
}