export interface Cuisine {
    id: string;
    name: string;
    createdAt: string;
    updatedAt: string;
}
