export interface ExtraMenuitem {
    id: string;
    name: string;
    description: string;
    weight: string;
    price: number;
    viewOrder: number;
    isHidden: boolean;

    _isEditing: boolean;
}