import { Menuitem } from "./menuitem.interface";

export interface Category {
    id: string;
    name: string;
    restaurationId: string;
    viewOrder: number;
    isStandingOffer: boolean;
    currentForDay?: string;
    menuitems: Menuitem[];

    // _isEditing: boolean;
    _isDraft: boolean;
}