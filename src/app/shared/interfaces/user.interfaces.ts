import { Restauration } from "./restauration.interface";
import { SecurityRole } from "./security-role.interface";

export interface User {
    id: string;
    login: string;
    email: string;
    firstName: string;
    lastName: string;
    phone: string;
    street: string;
    zipCode: string;
    city: string;
    state: string;
    restaurationId: string;
    restauration: Restauration;
    securityRoleId: string;
    securityRole: SecurityRole;
    stripeOnboarded: boolean;
}