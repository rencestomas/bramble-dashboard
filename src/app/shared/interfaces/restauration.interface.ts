import { Cuisine } from "./cuisine.interface";

export interface Restauration {
    id: string;
    name: string;
    email: string;
    description: string;
    phone: string;
    street: string;
    zipCode: string;
    city: string;
    state: string;
    isOpen: boolean;
    cuisines: Cuisine[];
    festivalRestaurants: any[];
}