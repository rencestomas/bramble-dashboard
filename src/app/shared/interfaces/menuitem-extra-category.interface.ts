import { ExtraCategory } from "./extra-category.interface";

export interface MenuItemExtraCategory {
    id: string;
    viewOrder: number;
    isHidden: boolean;
    isOneChoice: boolean;
    isMandatory: boolean;
    menuitemId: string;
    extraCategoryId: string;

    extraCategory: ExtraCategory;
    
    _isEditing: boolean;
    _isDeleted?: boolean;
}