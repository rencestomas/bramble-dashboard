import { ExtraMenuitem } from "./extra-menuitem.interface";

export interface ExtraCategoryExtraMenuitem {
    id: string;
    viewOrder: number;
    isHidden: boolean;
    extraMenuitemId: string;
    extraCategoryId: string;
    
    extraMenuitem: ExtraMenuitem;

    _isDeleted?: boolean;
}