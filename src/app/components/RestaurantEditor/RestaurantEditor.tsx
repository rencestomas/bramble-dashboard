import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from "react";
import { Button, Chip, Divider, Grid, IconButton, ImageList, ImageListItem, ImageListItemBar, ListSubheader, Select, Stack, styled, TextField, useTheme } from "@mui/material";
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import DeleteIcon from '@mui/icons-material/Delete';
import FileUploading from 'react-files-uploading';
import { Menuitem } from '../../shared/interfaces/menuitem.interface';
import useAxiosSecure from '../../../hooks/useAxiosSecure';
import { useSelector } from 'react-redux';
import { RootState } from '../../core/store/store';
import { ExtraCategory } from '../../shared/interfaces/extra-category.interface';
import { useTranslation } from 'react-multi-lang';
import { MINIO_URL } from '../../core/api/axios';
import { Restauration } from '../../shared/interfaces/restauration.interface';
import { useFormInputValidation } from 'react-form-input-validation';


const StyledTextField = styled(TextField)(({ theme }) => ({
    background: 'white',
    width: '100%'
}));

export interface RestaurantEditorProps {
    action: 'CREATE' | 'UPDATE' | 'VIEW' | null,
    data: Restauration;
    onChangeData: (data: any) => void;
    onDelete: (e: Restauration) => void;
}

export default function RestaurantEditor(props: RestaurantEditorProps) {

    const theme = useTheme();

    const [name, setName] = useState(props?.data?.name || "");
    const [description, setDescription] = useState(props?.data?.description || "");
    const [email, setEmail] = useState(props?.data?.email || "");
    const [phone, setPhone] = useState(props?.data?.phone || "");
    const [street, setStreet] = useState(props?.data?.street || "");
    const [zipCode, setZipCode] = useState(props?.data?.zipCode || "");
    const [city, setCity] = useState(props?.data?.city || "");
    const [state, setState] = useState(props?.data?.state || "");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [login, setLogin] = useState("");
    const [fileToUpload, setFileToUpload] = useState<File | null>(null);
    const [thumbnail, setThumbnail] = useState<string>("../../../../src/assets/restaurant_image_placeholder.png");


    const axiosSecure = useAxiosSecure();
    const auth = useSelector((state: RootState) => state.auth);
    const t = useTranslation();


    // useEffect(() => {
    //     async function apiCall() {

    //         let filter = "";

    //         if (auth.user) {
    //             filter += `restaurationId=${auth.user?.restaurationId}&`;
    //         }

    //     }

    //     apiCall();
    // }, [])


    useEffect(() => {

        setName(props?.data?.name || "");
        setDescription(props?.data?.description || "");
        setEmail(props?.data?.email || "");
        setPhone(props?.data?.phone || "");
        setStreet(props?.data?.street || "");
        setZipCode(props?.data?.zipCode || "");
        setCity(props?.data?.city || "");
        setState(props?.data?.state || "");
        setFirstName("");
        setLastName("");
        setLogin("");

        setFileToUpload(null);
        setThumbnail(`${MINIO_URL}/${auth.user?.restaurationId}/${props?.data?.id}.jpg`);

    }, [props?.data])


    useEffect(() => {

        props?.onChangeData({
            id: props?.data?.id,
            name,
            description,
            email,
            phone,
            street,
            zipCode,
            city,
            state,
            image: fileToUpload ? fileToUpload : null,
            login,
            firstName,
            lastName
        });

    }, [name, description, email, phone, street, zipCode, city, state, firstName, lastName, login, fileToUpload])


    function handleNameChange(event: any) {
        setName(event.target.value);
    }

    function handleDescriptionChange(event: any) {
        setDescription(event.target.value);
    }

    function handleEmailChange(event: any) {
        setEmail(event.target.value);
    }

    function handlePhoneChange(event: any) {
        setPhone(event.target.value);
    }

    function handleStreetChange(event: any) {
        setStreet(event.target.value);
    }

    function handleZipCodeChange(event: any) {
        setZipCode(event.target.value);
    }

    function handleCityChange(event: any) {
        setCity(event.target.value);
    }

    function handleStateChange(event: any) {
        setState(event.target.value);
    }

    function handleFirstNameChange(event: any) {
        setFirstName(event.target.value);
    }

    function handleLastNameChange(event: any) {
        setLastName(event.target.value);
    }

    function handleLoginChange(event: any) {
        setLogin(event.target.value);
    }

    function handleFileChange(fileList: File[]) {
        setFileToUpload(fileList[0]);
        const objectUrl = URL.createObjectURL(fileList[0]);
        setThumbnail(objectUrl);
        // setPreview(objectUrl)

        // free memory when ever this component is unmounted
        // return () => URL.revokeObjectURL(objectUrl)
    };

    function handleRemoveRestaurantThumbnailClick(event: any) {
        event.stopPropagation();
        setFileToUpload(null);
        setThumbnail('');
    }

    function handleDeleteRestaurant() {
        props?.onDelete(props?.data);
    }

    return (
        <Stack spacing={2}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Typography><strong>{t('Title')}</strong></Typography>
                    <StyledTextField
                        variant="outlined"
                        value={name}
                        onChange={handleNameChange}
                    />
                </Grid>

                <Grid item xs={8}>
                    <Typography><strong>{t('Description')}</strong></Typography>
                    <StyledTextField
                        variant="outlined"
                        multiline
                        rows={4}
                        value={description}
                        onChange={handleDescriptionChange}
                    />
                </Grid>

                <Grid item xs={4}>
                    <Typography><strong>{t('Image')}</strong></Typography>
                    <FileUploading value={fileToUpload != null ? [fileToUpload] : []} onChange={handleFileChange}
                        acceptType={['jpg', 'jpeg', 'png']} maxFileSize={2097152}>
                        {({
                            fileList,
                            errors,
                            isDragging,
                            onFileUpload,
                            onFileRemoveAll,
                            onFileUpdate,
                            onFileRemove,
                            dragProps,
                        }) => {
                            return (
                                <>
                                    <Box
                                        id="btn-upload"
                                        onClick={onFileUpload}
                                        {...dragProps}
                                        sx={{
                                            background: 'white',
                                            position: 'relative',
                                            color: '#AEAEB2', // theme.palette.primary.main,
                                            borderRadius: '8px',
                                            border: `3px dashed #AEAEB2`, // ${theme.palette.primary.main}`,
                                            height: '125px',
                                            textAlign: 'center',
                                            cursor: 'pointer',
                                            transition: 'filter ease-in-out 200ms',
                                            ':hover': {
                                                filter: 'brightness(0.85)'
                                            }
                                        }}
                                    >
                                        {errors && errors.maxNumber && (
                                            <span>Number of selected files exceed maxNumber</span>
                                        )}

                                        <>
                                            <Stack
                                                spacing={2}
                                                direction="column"
                                                justifyContent="space-between"
                                                sx={{
                                                    position: 'absolute',
                                                    zIndex: 1,
                                                    color: 'white',
                                                    left: '50%',
                                                    top: '50%',
                                                    transform: 'translate(-50%, -50%)'
                                                }}
                                            >
                                                <Typography
                                                    sx={{
                                                        textOverflow: 'ellipsis',
                                                        overflow: 'hidden',
                                                        whiteSpace: 'nowrap'
                                                    }}
                                                >
                                                    Click here or drop
                                                </Typography>

                                                {fileToUpload != null && (
                                                    <Stack
                                                        spacing={2}
                                                        direction="row"
                                                        justifyContent="space-between"
                                                    >
                                                        <Typography
                                                            sx={{
                                                                textOverflow: 'ellipsis',
                                                                overflow: 'hidden',
                                                                whiteSpace: 'nowrap',
                                                                maxWidth: '100px'
                                                            }}
                                                        >
                                                            {fileToUpload?.name}

                                                        </Typography>

                                                        <DeleteIcon
                                                            onClick={(e) => handleRemoveRestaurantThumbnailClick(e)} />
                                                    </Stack>)}
                                            </Stack>
                                            <img
                                                style={{
                                                    filter: "brightness(70%)",
                                                    borderRadius: "5px",
                                                    height: "100%",
                                                    width: "100%",
                                                    objectFit: "cover"
                                                }}
                                                src={thumbnail}
                                                onError={({ currentTarget }) => {
                                                    currentTarget.onerror = null; // prevents looping
                                                    currentTarget.src = require("../../../../src/assets/restaurant_image_placeholder.png");
                                                }}
                                            />

                                        </>

                                    </Box>
                                </>
                            );
                        }}
                    </FileUploading>
                </Grid>

                <Grid item xs={12}>
                    <Typography><strong>{t('Email')}</strong></Typography>
                    <StyledTextField
                        variant="outlined"
                        value={email}
                        onChange={handleEmailChange}
                    />
                </Grid>

                <Grid item xs={12}>
                    <Typography><strong>{t('Phone')}</strong></Typography>
                    <StyledTextField
                        variant="outlined"
                        value={phone}
                        onChange={handlePhoneChange}
                    />
                </Grid>

                <Grid item xs={12}>
                    <Typography><strong>{t('Street')}</strong></Typography>
                    <StyledTextField
                        variant="outlined"
                        value={street}
                        onChange={handleStreetChange}
                    />
                </Grid>

                <Grid item xs={12}>
                    <Typography><strong>{t('ZipCode')}</strong></Typography>
                    <StyledTextField
                        variant="outlined"
                        value={zipCode}
                        onChange={handleZipCodeChange}
                    />
                </Grid>

                <Grid item xs={12}>
                    <Typography><strong>{t('City')}</strong></Typography>
                    <StyledTextField
                        variant="outlined"
                        value={city}
                        onChange={handleCityChange}
                    />
                </Grid>

                <Grid item xs={12}>
                    <Typography><strong>{t('Country')}</strong></Typography>
                    <StyledTextField
                        required
                        variant="outlined"
                        value={state}
                        onChange={handleStateChange}
                    />
                </Grid>

                {props?.action === 'CREATE' && <>
                    <Grid item xs={12}><Typography><strong>{t('ManagerProfile')}</strong></Typography></Grid>

                    <Grid item xs={6}>
                        <Typography><strong>{t('FirstName')}</strong></Typography>
                        <StyledTextField
                            variant="outlined"
                            value={firstName}
                            onChange={handleFirstNameChange}
                        />
                    </Grid>

                    <Grid item xs={6}>
                        <Typography><strong>{t('LastName')}</strong></Typography>
                        <StyledTextField
                            variant="outlined"
                            value={lastName}
                            onChange={handleLastNameChange}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <Typography><strong>{t('ManagerLogin')}</strong></Typography>
                        <StyledTextField
                            variant="outlined"
                            value={login}
                            onChange={handleLoginChange}
                        />
                    </Grid>
                </>}
            </Grid>

            {props?.action === 'UPDATE' && <>
                <Divider />

                <Button
                    color="black"
                    variant="text"
                    onClick={handleDeleteRestaurant}
                >
                    {t('DeleteRestaurantAction')}
                </Button>
            </>}
        </Stack>
    );
}