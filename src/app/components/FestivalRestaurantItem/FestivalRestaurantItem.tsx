import { ListItem, Stack, Typography, useTheme } from "@mui/material";
import { useTranslation } from "react-multi-lang";
import { useDispatch } from "react-redux";
import DeleteIcon from '@mui/icons-material/Delete';


export interface FestivalRestaurantItemProps {
    festivalRestaurant: any;
    onClick: (e: any) => void;
    onDelete: (e: any) => void;
}

export default function FestivalRestaurantItem(props: FestivalRestaurantItemProps) {

    const theme = useTheme();
    const t = useTranslation()
    const dispatch = useDispatch();

    const iconStyle = {
        cursor: 'pointer'
    }

    function handleDeleteClick(event: any) {
        event.stopPropagation();
        props?.onDelete?.(props?.festivalRestaurant);
    }

    return (
        <ListItem sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            cursor: 'pointer',
            ':hover': {
                background: theme.palette.grey[100]
            }
        }}
            onClick={(e) => props?.onClick?.(props?.festivalRestaurant)}
        >
            <Stack
                justifyContent="space-between"
            >
                <Typography><strong>{props?.festivalRestaurant?.restaurantName}</strong></Typography>
                <Typography>{props?.festivalRestaurant?.location}</Typography>
            </Stack>

            <DeleteIcon sx={iconStyle} onClick={handleDeleteClick} />
        </ListItem>
    );
}