import {
    Box,
    Grid,
    Button,
    Card,
    CardContent,
    Chip,
    Divider,
    Stack,
    styled,
    Typography,
    useTheme
} from "@mui/material";

import { useState } from "react";
import { Order } from "../../shared/interfaces/order.interface";
import { useTranslation } from "react-multi-lang";
import StackGrid from "react-stack-grid";
import { MINIO_URL } from "../../core/api/axios";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../core/store/store";
import RejectOrderDialog, { RejectOrderDialogResult } from "../RejectOrderDialog/RejectOrderDialog";
import { OrderStateEnum } from "../../shared/interfaces/order-state.interface";
import useAxiosSecure from "../../../hooks/useAxiosSecure";
import { updateOrder } from "../../core/store/features/order/orderSlice";
import { setPanelContext } from "../../core/store/features/sidePanel/sidePanelSlice";


export interface OrderDetailsProps {
    data: Order;
    showActions?: boolean;
    onApproveAndPrepare?: (order: Order | null) => void;
    onDone?: (order: Order | null) => void;
    onCancel?: (order: Order | null, orderStateReason: string) => void;
}

const OrderCard = styled(Card)(({ theme }) => ({
    maxWidth: 275,
    borderRadius: "10px",
    boxShadow: '0px 2px 6px rgba(13, 10, 44, 0.08)',
    "& .MuiCardContent-root": {
        padding: "4px"
    }
}));

const BoldTypography = styled(Typography)(({ theme }) => ({
    fontWeight: 'bold'
}));


export default function OrderDetails(props: OrderDetailsProps) {

    const theme = useTheme();
    const { onApproveAndPrepare, onDone } = props;
    const [openConfirm, setOpenConfirm] = useState(false);
    const t = useTranslation();
    const auth = useSelector((state: RootState) => state.auth);
    const axiosSecure = useAxiosSecure();
    const dispatch = useDispatch();


    // const handleOpenConfirm = () => {
    //     setOpenConfirm(true);
    // };

    async function handleConfirmClose(value: RejectOrderDialogResult) {
        setOpenConfirm(false);

        let orderStateReason = "";

        switch (value.rejectReason) {
            case "1":
                orderStateReason = t('MenuitemOutOfStock');
                break;
            case "2":
                orderStateReason = t('OverloadedService');
                break;
            case "3":
                orderStateReason = t('TechnicalReason');
                break;
            case "4":
                orderStateReason = t('Other');
                break;
            default:
                orderStateReason = t('Other');
                break;
        }

        if (value.rejectOrder) {
            let response: any = await axiosSecure.put(`/api/order`, {
                id: props?.data?.id,
                orderStateId: OrderStateEnum.IsCanceled,
                orderStateReason: orderStateReason
            }, false);

            if (response?.data && response?.data?.length > 0) {
                dispatch(updateOrder(response?.data[0]));
                dispatch(setPanelContext({
                    action: 'VIEW',
                    contextType: 'Order',
                    contextObject: response?.data[0]
                }))
            }
        }
    };

    async function handleDone() {
        // props?.onDone?.(props.data);

        let response: any = await axiosSecure.put(`/api/order`, {
            id: props?.data?.id,
            orderStateId: OrderStateEnum.IsDone
        }, false);

        if (response?.data && response?.data?.length > 0) {
            dispatch(updateOrder(response?.data[0]));
            dispatch(setPanelContext({
                action: 'VIEW',
                contextType: 'Order',
                contextObject: response?.data[0]
            }))
        }
    }

    const handleCancel = () => {
        setOpenConfirm(true);
    }

    async function handleApproveAndPrepare() {
        // props?.onApproveAndPrepare?.(props.data);

        let response: any = await axiosSecure.put(`/api/order`, {
            id: props?.data?.id,
            orderStateId: OrderStateEnum.IsPreparing
        }, false);

        if (response?.data && response?.data?.length > 0) {
            dispatch(updateOrder(response?.data[0]));
            dispatch(setPanelContext({
                action: 'VIEW',
                contextType: 'Order',
                contextObject: response?.data[0]
            }))
        }
    }

    function getThumbnailURL(menuitemId: string) {
        return `${MINIO_URL}/${auth.user?.restaurationId}/${menuitemId}.jpg`;
    }

    return (
        <>

            {openConfirm && <RejectOrderDialog
                open={openConfirm}
                onClose={handleConfirmClose}
                title={t('RejectOrder')}
                content={t('RejectOrderMessage')}
                noLabel={t('KeepOrder')}
                yesLabel={t('Reject')}
                order={props.data}
            />}


            <Stack
                direction="column"
                spacing={2}
            >

                <Stack direction="row" spacing={2}>
                    <Stack direction="row" spacing={1}>
                        <BoldTypography sx={{
                            color: theme.palette.primary.main
                        }}>
                            {t('Table')}
                        </BoldTypography>

                        <Typography sx={{
                            color: theme.palette.primary.main
                        }}>
                            {props?.data?.orderNumber}
                        </Typography>
                    </Stack>

                    <Stack direction="row" spacing={1}>
                        <BoldTypography>
                            {t('OrderedAgo')}:
                        </BoldTypography>

                        <Typography>
                            {new Date(props?.data?.createdAt).toLocaleTimeString()}
                        </Typography>
                    </Stack>

                    <Stack direction="row" spacing={1}>
                        <BoldTypography>
                            {t('Paid')}:
                        </BoldTypography>

                        <Typography>
                            {props?.data?.price} €
                        </Typography>
                    </Stack>
                </Stack>

                {props?.showActions && <Stack
                    direction="row"
                    spacing={1}
                >
                    {props?.data?.orderStateId === OrderStateEnum.IsPreparing ? (
                        <>
                            <Button
                                color="secondary"
                                variant="contained"
                                size="small"
                                autoFocus
                                onClick={handleDone}
                            >
                                {t('Done')}
                            </Button>
                        </>
                    ) : (
                        <>
                            <Button
                                color="black"
                                variant="contained"
                                size="small"
                                onClick={handleCancel}
                            >
                                {t('Reject')}
                            </Button>
                            <Button
                                variant="contained"
                                size="small"
                                autoFocus
                                onClick={handleApproveAndPrepare}
                            >
                                {t('AddOrder')}
                            </Button>
                        </>
                    )}
                </Stack>}
            </Stack>

            {props?.data != null && <>

                <Box
                    sx={{
                        position: 'relative',
                        zIndex: 0
                    }}
                >

                    <Box
                        sx={{
                            overflowY: 'auto',
                            marginBottom: '60px',
                            padding: "24px"
                        }}
                    >
                        {/* <OrdersGrid> */}
                        <StackGrid columnWidth={250} gutterHeight={16} gutterWidth={16}>

                            {props?.data?.orderMenuitems?.map((orderMenuitem: any, index: number) => (

                                <Grid item xs key={index}>
                                    <OrderCard>
                                        <CardContent>
                                            <Stack spacing={1} sx={{
                                                padding: "24px"
                                            }}>

                                                <Box sx={{
                                                    width: "50px",
                                                    height: "50px",
                                                    borderRadius: "20px"
                                                }}>
                                                    <img
                                                        style={{
                                                            borderRadius: "5px",
                                                            width: "40px",
                                                            height: "40px",
                                                            objectFit: "cover"
                                                        }}
                                                        src={getThumbnailURL(orderMenuitem?.menuitem?.id)}
                                                        onError={({ currentTarget }) => {
                                                            currentTarget.onerror = null; // prevents looping
                                                            currentTarget.src = require("../../../../src/assets/menuitem_placeholder.png");
                                                        }}
                                                    />
                                                </Box>

                                                <BoldTypography>
                                                    {orderMenuitem?.menuitem?.name}
                                                </BoldTypography>

                                                <Stack direction="row" justifyContent="space-between" spacing={1}>
                                                    <Typography>
                                                        {t('Quantity')}
                                                    </Typography>
                                                    <Typography sx={{ color: theme.palette.primary.main }}>
                                                        {orderMenuitem?.amount}x
                                                    </Typography>
                                                </Stack>

                                                <Divider />

                                                {orderMenuitem?.note &&
                                                    <>
                                                        <Stack direction="column" spacing={1}>
                                                            <BoldTypography>
                                                                {t('Note')}
                                                            </BoldTypography>

                                                            <Typography
                                                                sx={{
                                                                    maxWidth: 200,
                                                                    textOverflow: 'ellipsis',
                                                                    overflow: 'hidden',
                                                                    whiteSpace: 'nowrap'
                                                                }}
                                                            >
                                                                {orderMenuitem?.note}
                                                            </Typography>
                                                        </Stack>

                                                        <Divider />
                                                    </>
                                                }

                                                <BoldTypography>
                                                    Variant
                                                </BoldTypography>

                                                <Stack spacing={1}>
                                                    {orderMenuitem?.orderMenuitemExtraMenuitems?.map((orderMenuitemExtraMenuitem: any, index: number) => (
                                                        <Chip
                                                            key={orderMenuitemExtraMenuitem.id}
                                                            label={orderMenuitemExtraMenuitem.extraMenuitem.name}
                                                            sx={{
                                                                background: theme.palette.natural.main,
                                                                color: 'white'
                                                            }}
                                                        ></Chip>
                                                    ))}
                                                </Stack>

                                                <Stack direction="row" justifyContent="space-between" spacing={1}>
                                                    <Typography>
                                                        {t('TotalPrice')}
                                                    </Typography>

                                                    <BoldTypography>
                                                        {Math.round((orderMenuitem?.menuitem?.price * orderMenuitem?.amount + Number.EPSILON) * 100) / 100} €
                                                        +
                                                        {orderMenuitem?.extraMenuitemsPrice} €
                                                    </BoldTypography>
                                                </Stack>
                                            </Stack>

                                        </CardContent>
                                    </OrderCard>
                                </Grid>
                            ))}
                        </StackGrid>
                        {/* </OrdersGrid> */}
                    </Box>

                </Box>
            </>
            }
        </>
    );
}