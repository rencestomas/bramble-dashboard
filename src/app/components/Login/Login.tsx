import { Box, Button, Link, Stack, TextField, Typography } from "@mui/material";
import { useFormInputValidation } from "react-form-input-validation";
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from "react";
import { RootState } from "../../core/store/store";
import { useNavigate } from "react-router-dom";
import LanguageSelect from "../LanguageSelect/LanguageSelect";
import { setLanguage, useTranslation } from "react-multi-lang";
import { login } from "../../core/store/features/auth/authService";
import { updateAuth } from "../../core/store/features/auth/authSlice";
import { SecurityRole } from "../../core/security/SecurityRole";


export default function Login() {

    const dispatch = useDispatch();
    const auth = useSelector((state: RootState) => state.auth);
    const navigate = useNavigate();
    const t = useTranslation();


    const [fields, errors, form] = useFormInputValidation({
        login: "",
        password: "",
    }, {
        login: "required",
        password: "required"
    });

    async function onSubmit(event: any) {

        const isValid = await form.validate(event);
        if (isValid) {
            const response = await login({
                login: (fields as any).login,
                password: (fields as any).password
            });

            const accessToken = response?.accessToken;
            const refreshToken = response?.refreshToken;
            const user = response?.user;
            const restauration = user?.restauration;

            dispatch(updateAuth({
                accessToken,
                refreshToken,
                user,
                restauration
            }));
            if (!user?.stripeOnboarded) {
                new Promise((resolve, reject) => {
                    // wait 1 second before resolving
                    setTimeout(() => {
                        resolve('');
                    }, 100);
                }).then(() => {
                    navigate("/stripe-connect");
                });
            }

        }
    }

    useEffect(() => {
        let isLoggedIn = auth.user !== null;

        if (isLoggedIn) {
            if (auth.user?.securityRoleId === SecurityRole.Admin) {
                navigate("/admin");
            } else {
                navigate("/overview");
            }
        }
    })

    function handleLanguageChange(language: string) {
        setLanguage(language?.toLocaleLowerCase());
    }

    return (
        <>
            <Stack
                spacing={2}
                alignItems="center"
                sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <Stack>
                    <LanguageSelect
                        text={t('Language')}
                        open={true}
                        onMouseOver={() => { }}
                        onMouseOut={() => { }}
                    />
                </Stack>

                <Box
                    component="form"
                    noValidate
                    onSubmit={onSubmit}
                    autoComplete="off"
                >
                    <Stack spacing={2} alignItems="center">

                        <TextField
                            required
                            label={t('UserName')}
                            name="login"
                            variant="outlined"
                            onBlur={form.handleBlurEvent}
                            onChange={form.handleChangeEvent}
                            value={(fields as any).login}
                        />
                        <Typography
                            className="error"
                            sx={{
                                fontSize: 'small',
                                fontWeight: 'bold'
                            }}
                        >
                            {(errors as any).login
                                ? (errors as any).login
                                : ""}
                        </Typography>

                        <TextField
                            required
                            label={t('Password')}
                            name="password"
                            type="password"
                            variant="outlined"
                            onBlur={form.handleBlurEvent}
                            onChange={form.handleChangeEvent}
                            value={(fields as any).password}
                        />
                        <Typography
                            className="error"
                            sx={{
                                fontSize: 'small',
                                fontWeight: 'bold'
                            }}>
                            {(errors as any).password
                                ? (errors as any).password
                                : ""}
                        </Typography>

                        {/* <input type="submit" value="Submit" /> */}
                        <Button variant="contained" type="submit" value="Submit">
                            {t('Login')}
                        </Button>

                        <Typography>
                            {t('RegisterLabel')} <Link href="/register">{t('Register')}</Link>
                        </Typography>

                        <Typography>
                            <Link href="/festival/register">Register Festival</Link>
                        </Typography>

                        <Typography>
                            <Link href="/restaurant/register">Register Restaurant</Link>
                        </Typography>
                    </Stack>
                </Box>
            </Stack>
        </>

    );
}