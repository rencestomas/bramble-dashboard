import React, { FC, useState } from 'react';
import {
    DragDropContext,
    Droppable,
    Draggable,
    DropResult
} from 'react-beautiful-dnd';
import { Accordion, AccordionSummary, List, ListItem, Stack, styled, Typography, useTheme } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import DragIndicatorIcon from '@mui/icons-material/DragIndicator';
import EditIcon from '@mui/icons-material/Edit';
import VisibilityOutlinedIcon from '@mui/icons-material/VisibilityOutlined';
import VisibilityOffOutlinedIcon from '@mui/icons-material/VisibilityOffOutlined';
import { newGuid, reorder } from '../../core/utilities';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import DeleteSafeDialog from '../DeleteSafeDialog/DeleteSafeDialog';
import useAxiosSecure from '../../../hooks/useAxiosSecure';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../core/store/store';
import { ExtraCategory } from '../../shared/interfaces/extra-category.interface';
import { ExtraMenuitem } from '../../shared/interfaces/extra-menuitem.interface';
import CreateMenuItemButton from '../CreateMenuItemButton/CreateMenuItemButton';
import { ExtraCategoryExtraMenuitem } from '../../shared/interfaces/extra-category-extra-menuitem.interface';
import FilterNoneIcon from '@mui/icons-material/FilterNone';
import { useTranslation } from 'react-multi-lang';
import ContextMenu, { MenuData } from '../ContextMenu/ContextMenu';
import { addExtraCategory, deleteExtraCategory, removeExtraMenuitem, updateExtraCategories, updateExtraMenuitem, updateExtraMenuitems } from '../../core/store/features/extraCategory/extraCategorySlice';
import { setPanelContext } from '../../core/store/features/sidePanel/sidePanelSlice';


const RoundedAccordion = styled(Accordion)(() => ({
    marginBottom: '0.7rem',
    borderRadius: '0.6rem !important',
    '&:before': {
        display: 'none',
    },
    width: '100%'
}));

const RoundedAccordionSummary = styled(AccordionSummary)(({ theme }) => ({
    color: 'black',
    borderRadius: '8px',
    '&.Mui-expanded': {
        background: theme.palette.primary.main,
        color: 'white'
    },
    '.MuiAccordionSummary-expandIconWrapper': {
        color: 'black !important'
    },
    '.MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
        color: 'white'
    }
}));

export type ExtraCategoryListProps = {
    extraCategories: ExtraCategory[];
};

const ExtraCategoryList: FC<ExtraCategoryListProps> = React.memo(({
    extraCategories
}) => {

    const theme = useTheme();
    const [openSafeConfirm, setOpenSafeConfirm] = useState(false);
    const [toDeleteItem, setToDeleteItem] = useState<ExtraCategoryExtraMenuitem | ExtraCategory | null>(null);
    const [contextMenuData, setContextMenuData] = React.useState<MenuData | null>(null);


    const axiosSecure = useAxiosSecure();
    const auth = useSelector((state: RootState) => state.auth);
    const dispatch = useDispatch();
    const t = useTranslation();

    const iconStyle = {
        cursor: 'pointer'
    }

    // componentWillUnmount
    // useEffect(
    //     () => {
    //         return () => {
    //             if (draftExtraCategoriesRef.current) {
    //                 props?.onCreateDrafts(draftExtraCategoriesRef.current);
    //             }
    //         }
    //     },
    //     []
    // );

    async function onDragExtraCategoryEnd(e: DropResult) {
        // dropped outside the list
        const { destination, source, draggableId } = e;

        if (!destination) return;

        if (destination.index !== source.index) {
            const newItems = reorder(extraCategories, source.index, destination.index);

            dispatch(updateExtraCategories(
                newItems?.map((c, index) => ({
                    ...c,
                    viewOrder: index
                }))
            ));

            let response = await axiosSecure.put(`/api/extra_category/many`,
                newItems?.map((c, index) => ({
                    id: c?.id,
                    viewOrder: index
                }))
            );
        }

    };

    async function onDragExtraMenuitemEnd(e: DropResult, extraCategory: ExtraCategory) {
        // dropped outside the list
        const { destination, source, draggableId } = e;

        if (!destination) return;

        if (destination.index !== source.index) {

            let newItems: ExtraCategoryExtraMenuitem[] = reorder(extraCategory?.extraCategoryExtraMenuitems, source.index, destination.index);

            dispatch(updateExtraMenuitems({
                extraCategoryId: extraCategory?.id,
                extraCategoryExtraMenuitems: newItems?.map((c, index) => ({
                    ...c,
                    viewOrder: index
                }))
            }));

            let response = await axiosSecure.put(`/api/extra_category_extra_menuitem/many`,
                newItems?.map((c, index) => ({
                    id: c?.id,
                    viewOrder: index
                }))
            );

        }
    };

    function handleCreateNewMenuItemClick(event: any, extraCategory: ExtraCategory) {
        event.stopPropagation();

        dispatch(setPanelContext({
            action: 'CREATE', contextType: 'ExtraMenuitem', contextObject: {
                id: newGuid(),
                extraCategory,
                extraCategoryId: extraCategory.id
            },
            extraData: extraCategory
        }));
    }

    async function handleVisibilityExtraMenuitemClick(event: any, extraCategoryExtraMenuitem: ExtraCategoryExtraMenuitem) {
        event.stopPropagation();

        dispatch(updateExtraMenuitem({ ...extraCategoryExtraMenuitem, isHidden: !extraCategoryExtraMenuitem.isHidden }));

        let response = await axiosSecure.put(`/api/extra_category_extra_menuitem`, {
            id: extraCategoryExtraMenuitem?.id,
            isHidden: !extraCategoryExtraMenuitem.isHidden
        });
    }

    function handleOpenSafeConfirm(event: any, item: ExtraCategoryExtraMenuitem | ExtraCategory | null) {
        event.stopPropagation();
        setToDeleteItem(item);
        setOpenSafeConfirm(true);
    }

    async function handleSafeConfirmClose(event: any) {
        if (event) {
            if (toDeleteItem) {
                // delete extraMenuitem
                if ("extraCategoryId" in toDeleteItem) {
                    axiosSecure.delete(`/api/extra_category_extra_menuitem/${toDeleteItem?.id}`, true);
                    dispatch(removeExtraMenuitem(toDeleteItem));
                }
                // delete extraCategory
                else {
                    axiosSecure.delete(`/api/extra_category/${toDeleteItem?.id}`, true);
                    dispatch(deleteExtraCategory(toDeleteItem?.id))
                }
            }
        }

        setToDeleteItem(null);
        setOpenSafeConfirm(false);
    }

    function handleEditExtraCategoryClick(event: any, extraCategory: ExtraCategory) {
        event.stopPropagation();
        dispatch(setPanelContext({ action: 'UPDATE', contextType: 'ExtraCategory', contextObject: extraCategory }));
    }

    function handleEditExtraMenuitemClick(event: any, extraMenuitem: ExtraMenuitem) {
        event.stopPropagation();
        dispatch(setPanelContext({ action: 'UPDATE', contextType: 'ExtraMenuitem', contextObject: extraMenuitem }));
    }

    const handleContextMenu = (event: React.MouseEvent, data: ExtraCategoryExtraMenuitem | ExtraCategory, dataType: 'ExtraCategory' | 'ExtraCategoryExtraMenuitem') => {
        event.preventDefault();
        setContextMenuData(
            contextMenuData === null
                ? {
                    mouseX: event.clientX + 2,
                    mouseY: event.clientY - 6,
                    data,
                    dataType
                }
                : // repeated contextmenu when it is already open closes it with Chrome 84 on Ubuntu
                // Other native context menus might behave different.
                // With this behavior we prevent contextmenu from the backdrop to re-locale existing context menus.
                null,
        );
    };

    const handleContextMenuClose = () => {
        setContextMenuData(null);
    };

    function handleDeleteActionClick(event: any, item: any, data: any) {
        handleOpenSafeConfirm(event, data)
    }

    async function handleDuplicateActionClick(event: any, item: any, data: any, dataType: any) {

        if (dataType === 'ExtraCategoryExtraMenuitem') {
            // TODO ????
            // let dataChange = { ...data, id: newGuid() };

            // let response = await axiosSecure.post(`/api/extra_category_extra_menuitem`, dataChange, true);

            // if (response?.data && response?.data?.length > 0) {
            //     dispatch(addExtraMenuitem(response?.data[0]));
            // }
        }

        if (dataType === 'ExtraCategory') {
            let dataChange = { ...data, id: newGuid() };

            let response = await axiosSecure.post(`/api/extra_category`, dataChange, true);

            if (response?.data && response?.data?.length > 0) {
                dispatch(addExtraCategory(response?.data[0]));
            }
        }

    }

    async function handleDuplicateExtraCategoryClick(event: any, extraCategory: ExtraCategory) {
        event.stopPropagation();

        let dataChange = { ...extraCategory, id: newGuid() };

        let response: any = await axiosSecure.post(`/api/extra_category`, dataChange, false);
        if (response?.data && response?.data?.length > 0) {
            dispatch(addExtraCategory(response?.data[0]));
        }
    }

    return (
        <>
            <ContextMenu
                data={contextMenuData}
                items={
                    contextMenuData?.dataType === 'ExtraCategory' ? [
                        {
                            icon: <FilterNoneIcon fontSize="small" />,
                            label: t("Duplicate"),
                            type: 'actionItem',
                            onAction: handleDuplicateActionClick
                        },
                        {
                            type: 'divider'
                        },
                        {
                            icon: <DeleteOutlineIcon fontSize="small" />,
                            label: t("DeleteAction"),
                            type: 'actionItem',
                            onAction: handleDeleteActionClick
                        }
                    ] : [
                        {
                            icon: <DeleteOutlineIcon fontSize="small" />,
                            label: t("DeleteAction"),
                            type: 'actionItem',
                            onAction: handleDeleteActionClick
                        }
                    ]
                }
                open={contextMenuData !== null}
                onClose={handleContextMenuClose}
            />

            <DeleteSafeDialog
                open={openSafeConfirm}
                onClose={(e) => handleSafeConfirmClose(e)}
                title={t('DeleteMenuitemQuestion')}
                content={t('DeleteMenuitemQuestionContent')}
                confirmPhrase={((toDeleteItem && "extraCategoryId" in toDeleteItem) ? toDeleteItem?.extraMenuitem?.name : toDeleteItem?.name) || ''}
                noLabel={t('KeepAction')}
                yesLabel={t('DeleteAction')}
            />

            {/* {(!menuitem?.menuitemExtraCategories || menuitem?.menuitemExtraCategories?.length === 0) && <Alert severity="info"
                sx={{
                    marginTop: '10px'
                }}
            >
                Vďaka extra možnostiam si vaši hostia môžu prispôsobiť položku presne podľa svojej chuti. <b>Tento krok je nepovinný. Môžete ho preskočiť a kedykoľvek sa k nemu vrátiť.</b>
            </Alert>}


            {menuitem?.menuitemExtraCategories && menuitem?.menuitemExtraCategories?.length > 0 && <Alert severity="info"
                sx={{
                    marginTop: '10px'
                }}
            >
                Medzi povinné položky s jednou voľbou patrí napríklad úroveň štipľavosti (nepikantné / jemne pikantné / pikantné). Medzi nepovinné položky patria napríklad extra ingrediencie, ktoré si hosť môže pridať (kukurica / olivy / chilli)
            </Alert>} */}

            <DragDropContext onDragEnd={(e) => onDragExtraCategoryEnd(e)}>
                <Droppable droppableId="extra-droppable-list">
                    {(provided) => (
                        <List ref={provided.innerRef} {...provided.droppableProps}
                            sx={{
                                paddingTop: '0px'
                            }}
                        >
                            {extraCategories?.map((extraCategory, index) => (
                                <Draggable draggableId={extraCategory.id} index={index} key={extraCategory.id}>
                                    {(provided, snapshot) => (
                                        <RoundedAccordion
                                            disableGutters
                                            elevation={0}
                                            sx={{
                                                boxShadow: snapshot.isDragging ? "0px 8px 9px -5px rgb(0 0 0 / 20%), 0px 15px 22px 2px rgb(0 0 0 / 14%), 0px 6px 28px 5px rgb(0 0 0 / 12%);" : "initial"
                                            }}
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                        >
                                            <RoundedAccordionSummary
                                                expandIcon={<ExpandMoreIcon />}
                                                sx={{
                                                    color: 'black',
                                                    borderRadius: '8px',
                                                    '&.Mui-expanded': {
                                                        background: theme.palette.primary.main,
                                                        color: 'white'
                                                    },
                                                    '.MuiAccordionSummary-expandIconWrapper': {
                                                        color: 'black !important'
                                                    },
                                                    '.MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
                                                        color: 'white !important'
                                                    }
                                                }}
                                                onContextMenu={(e: any) => handleContextMenu(e, extraCategory, 'ExtraCategory')}
                                            >
                                                <Stack
                                                    spacing={2}
                                                    direction="row"
                                                    justifyContent="space-between"
                                                    alignItems="center"
                                                    sx={{
                                                        width: '100%',
                                                        height: '20px'
                                                    }}
                                                >
                                                    <div {...provided.dragHandleProps}>
                                                        <DragIndicatorIcon />
                                                    </div>

                                                    <Typography>
                                                        <strong>{extraCategory.name}</strong>
                                                    </Typography>


                                                    <Stack
                                                        spacing={2}
                                                        direction="row"
                                                        justifyContent="space-between"
                                                        alignItems="center"
                                                    >

                                                        <EditIcon
                                                            sx={iconStyle}
                                                            fontSize="small"
                                                            onClick={(e) => handleEditExtraCategoryClick(e, extraCategory)}
                                                        />

                                                        <FilterNoneIcon
                                                            sx={iconStyle}
                                                            fontSize="small"
                                                            onClick={(e) => handleDuplicateExtraCategoryClick(e, extraCategory)}
                                                        />

                                                    </Stack>
                                                </Stack>

                                            </RoundedAccordionSummary>


                                            <Stack
                                                direction="row"
                                                justifyContent="space-around"
                                                sx={{
                                                    mt: 2
                                                }}
                                            >
                                                <CreateMenuItemButton
                                                    onClick={(e) => handleCreateNewMenuItemClick(e, extraCategory)}
                                                />
                                            </Stack>

                                            <DragDropContext onDragEnd={(event) => onDragExtraMenuitemEnd(event, extraCategory)}>
                                                <Droppable droppableId={"extra-menu-items-list" + extraCategory?.id}>
                                                    {(provided2) => (
                                                        <List ref={provided2.innerRef} {...provided2.droppableProps}>
                                                            {extraCategory?.extraCategoryExtraMenuitems?.map((extraCategoryExtraMenuitem: any, index2: number) => (
                                                                <Draggable draggableId={extraCategoryExtraMenuitem.id} index={index2} key={extraCategoryExtraMenuitem.id}>
                                                                    {(provided2, snapshot2) => (
                                                                        <ListItem
                                                                            sx={{
                                                                                height: '80px',
                                                                                color: extraCategoryExtraMenuitem?.isHidden ? '#8E8E93' : 'black'
                                                                            }}
                                                                            ref={provided2.innerRef}
                                                                            {...provided2.draggableProps}
                                                                            onContextMenu={(e: any) => handleContextMenu(e, extraCategoryExtraMenuitem, 'ExtraCategoryExtraMenuitem')}
                                                                        >
                                                                            <Stack
                                                                                spacing={2}
                                                                                direction="row"
                                                                                justifyContent="space-between"
                                                                                sx={{
                                                                                    width: '100%',
                                                                                    height: '50px',
                                                                                    borderRadius: '0.6rem',
                                                                                    bgcolor: 'white',
                                                                                    boxShadow: snapshot2.isDragging ? "0px 8px 9px -5px rgb(0 0 0 / 20%), 0px 15px 22px 2px rgb(0 0 0 / 14%), 0px 6px 28px 5px rgb(0 0 0 / 12%);" : "initial"
                                                                                }}
                                                                            >
                                                                                <Stack
                                                                                    direction="row"
                                                                                    alignItems="center"
                                                                                    spacing={2}
                                                                                    sx={{
                                                                                        overflow: 'hidden'
                                                                                    }}
                                                                                >
                                                                                    <div
                                                                                        {...provided2.dragHandleProps}
                                                                                    >
                                                                                        <DragIndicatorIcon fontSize="small" />
                                                                                    </div>
                                                                                    <Typography fontSize="small">
                                                                                        <strong>{extraCategoryExtraMenuitem?.extraMenuitem?.name}</strong>
                                                                                    </Typography>
                                                                                    <Typography fontSize="small">
                                                                                        <strong>{extraCategoryExtraMenuitem?.extraMenuitem?.weight}</strong>
                                                                                    </Typography>
                                                                                </Stack>

                                                                                <Stack direction="row" alignItems="center" spacing={2}>

                                                                                    <Typography fontSize="small">
                                                                                        <strong>{extraCategoryExtraMenuitem?.extraMenuitem.price}€</strong>
                                                                                    </Typography>

                                                                                    <EditIcon
                                                                                        sx={iconStyle}
                                                                                        fontSize="small"
                                                                                        onClick={(e) => handleEditExtraMenuitemClick(e, extraCategoryExtraMenuitem?.extraMenuitem)}
                                                                                    />

                                                                                    <DeleteOutlineIcon
                                                                                        sx={iconStyle}
                                                                                        fontSize="small"
                                                                                        onClick={(e) => handleOpenSafeConfirm(e, extraCategoryExtraMenuitem)}
                                                                                    />

                                                                                    {extraCategoryExtraMenuitem?.isHidden ? (<VisibilityOffOutlinedIcon
                                                                                        sx={iconStyle}
                                                                                        fontSize="small"
                                                                                        onClick={(e) => handleVisibilityExtraMenuitemClick(e, extraCategoryExtraMenuitem)}
                                                                                    />) : (<VisibilityOutlinedIcon
                                                                                        sx={iconStyle}
                                                                                        fontSize="small"
                                                                                        onClick={(e) => handleVisibilityExtraMenuitemClick(e, extraCategoryExtraMenuitem)}
                                                                                    />)}

                                                                                </Stack>
                                                                            </Stack>
                                                                        </ListItem>
                                                                    )}
                                                                </Draggable>
                                                            ))}
                                                            {provided2.placeholder}
                                                        </List>
                                                    )}
                                                </Droppable>
                                            </DragDropContext>

                                        </RoundedAccordion>
                                    )}
                                </Draggable>
                            ))}
                            {provided.placeholder}
                        </List>
                    )}
                </Droppable>
            </DragDropContext >
        </>
    );
});

export default ExtraCategoryList;