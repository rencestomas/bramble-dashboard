import { Box, Button, Grid, Link, Stack, styled, TextField, Typography } from "@mui/material";
import { useFormInputValidation } from "react-form-input-validation";
import { useTranslation } from "react-multi-lang";
import { useDispatch } from 'react-redux';
import { useNavigate } from "react-router-dom";
import { register } from "../../core/store/features/auth/authService";
import { displayNotification } from "../../core/store/features/notification/notificationSlice";
import { newGuid } from "../../core/utilities";
import LanguageSelect from "../LanguageSelect/LanguageSelect";


const SmallBoldTypography = styled(Typography)(({ theme }) => ({
    fontSize: 'small',
    fontWeight: 'bold'
}));

export default function Register() {

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const t = useTranslation();


    const [fields, errors, form] = useFormInputValidation({
        login: "",
        email: "",
        password1: "",
        password2: "",
        firstName: "",
        lastName: "",
        phone: "",
        street: "",
        zipCode: "",
        city: "",
        state: "SK"
    }, {
        login: "required",
        email: "required|email",
        password1: "required",
        password2: "required",
        firstName: "required",
        lastName: "required",
        phone: "required",
        street: "required",
        zipCode: "required",
        city: "required",
        state: "required"
    });

    async function onSubmit(event: any) {
        const isValid = await form.validate(event);

        if (isValid) {
            const response = await register({
                id: newGuid(),
                login: (fields as any).login,
                email: (fields as any).email,
                password1: (fields as any).password1,
                password2: (fields as any).password2,
                firstName: (fields as any).firstName,
                lastName: (fields as any).lastName,
                phone: (fields as any).phone,
                street: (fields as any).street,
                zipCode: (fields as any).zipCode,
                city: (fields as any).city,
                state: (fields as any).state,
                securityRoleId: "0e82dd1c-9fc8-4fd3-a922-a835650fa09b"
            });

            dispatch(displayNotification({
                message: t('AccountCreated'),
                autoHideDuration: 5000,
                severity: "success"
            }));

            navigate("/login");

            // const accessToken = response?.accessToken;
            // const refreshToken = response?.refreshToken;
            // const user = response?.user;


            // dispatch(updateAuth({
            //     accessToken,
            //     refreshToken,
            //     user
            // }));
        }

    }

    return (
        <Box
            sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
            }}
        >
            <Stack>
                <LanguageSelect
                    text={t('Language')}
                    open={true}
                    onMouseOver={() => { }}
                    onMouseOut={() => { }}
                />

                <Grid
                    container
                    rowSpacing={4}
                    columnSpacing={4}
                    component="form"
                    onSubmit={onSubmit}
                    autoComplete="off"
                >
                    <Grid container item xs={6} direction="column" >
                        <Stack spacing={2}>
                            <TextField
                                required
                                label={t("UserName")}
                                name="login"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).login}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).login
                                    ? (errors as any).login
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label="Email"
                                name="email"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).email}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).email
                                    ? (errors as any).email
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("Password")}
                                name="password1"
                                type="password"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).password1}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).password
                                    ? (errors as any).password
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("RepeatPassword")}
                                name="password2"
                                type="password"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).password2}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).password2
                                    ? (errors as any).password2
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("FirstName")}
                                name="firstName"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).firstName}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).firstName
                                    ? (errors as any).firstName
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("LastName")}
                                name="lastName"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).lastName}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).lastName
                                    ? (errors as any).lastName
                                    : ""}
                            </SmallBoldTypography>
                        </Stack>
                    </Grid>
                    <Grid container item xs={6} direction="column" >
                        <Stack spacing={2}>
                            <TextField
                                required
                                label={t("PhoneNumber")}
                                name="phone"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).phone}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).phone
                                    ? (errors as any).phone
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("Country")}
                                name="state"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).state}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).state
                                    ? (errors as any).state
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("City")}
                                name="city"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).city}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).city
                                    ? (errors as any).city
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("ZipCode")}
                                name="zipCode"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).zipCode}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).zipCode
                                    ? (errors as any).zipCode
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("Street")}
                                name="street"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).street}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).street
                                    ? (errors as any).street
                                    : ""}
                            </SmallBoldTypography>
                        </Stack>
                    </Grid>

                    <Box
                        sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            width: "100%"
                        }}
                    >
                        <Stack spacing={2}>
                            <Button type="submit" variant="contained" value="Submit">
                                {t('CreateAccount')}
                            </Button>

                            <Typography>
                                {t('LoginLabel')} <Link href="/login">{t('LoginAction')}</Link>
                            </Typography>
                        </Stack>
                    </Box>
                </Grid>
            </Stack>
        </Box>
    );
}