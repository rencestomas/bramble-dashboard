import { Card, CardContent, CardMedia, Typography, useTheme } from "@mui/material";
import { useEffect, useState } from "react";
import { useTranslation } from "react-multi-lang";
import { setPanelContext } from "../../core/store/features/sidePanel/sidePanelSlice";
import { useDispatch } from "react-redux";
import { Restauration } from "../../shared/interfaces/restauration.interface";
import { MINIO_URL } from "../../core/api/axios";


export interface RestaurantItemProps {
    restaurant: Restauration;
}

export default function RestaurantItem(props: RestaurantItemProps) {

    const theme = useTheme();
    const t = useTranslation()
    const dispatch = useDispatch();
    const [thumbnail, setThumbnail] = useState<string>("../../../../src/assets/restaurant_image_placeholder.png");


    useEffect(() => {
        setThumbnail(`${MINIO_URL}/restaurants/${props?.restaurant?.id}.jpg`);
    }, [props?.restaurant])

    const handleClick = () => {
        dispatch(setPanelContext({
            action: 'UPDATE',
            contextType: 'Restaurant',
            contextObject: props?.restaurant
        }));
    };

    return (
        <>
            <Card sx={{
                aspectRatio: "1/ 1",
                maxHeight: "300px",
                borderRadius: "10px",
                textAlign: "center",
                margin: "5px",
                display: 'flex',
                justiyContent: 'space-between',
                flexDirection: 'column',
                boxShadow: '0px 2px 6px rgba(13, 10, 44, 0.08)',
                ':hover': {
                    boxShadow: 15,
                    cursor: 'pointer'
                }
            }}
                onClick={handleClick}
            >
                <CardMedia
                    component="img"
                    height="195"
                    image={thumbnail}
                    alt={props?.restaurant?.name}
                    onError={({ currentTarget }) => {
                        currentTarget.onerror = null; // prevents looping
                        currentTarget.src = require("../../../../src/assets/restaurant_image_placeholder.png");
                    }}
                />
                <CardContent
                    sx={{
                        height: "100%"
                    }}
                >
                    <Typography
                        sx={{
                            fontWeight: 'bold'
                        }}
                    >
                        {props?.restaurant?.name}
                    </Typography>

                </CardContent>
            </Card>
        </>
    );
}