import { useEffect, useState } from 'react';
import { Box, Button, Divider, FormControl, FormControlLabel, InputLabel, MenuItem, Radio, RadioGroup, Select, SelectChangeEvent, Stack, TextField, useTheme } from '@mui/material';
import { ExtraMenuitem } from '../../shared/interfaces/extra-menuitem.interface';
import useAxiosSecure from '../../../hooks/useAxiosSecure';
import { useSelector } from 'react-redux';
import { RootState } from '../../core/store/store';
import { ExtraCategory } from '../../shared/interfaces/extra-category.interface';
import { newGuid } from '../../core/utilities';

export interface ExtraMenuItemEditorProps {
    action: 'CREATE' | 'UPDATE' | 'VIEW' | null,
    data: ExtraMenuitem;
    extraCategory: ExtraCategory | null;
    onChangeData: (data: any) => void;
    onDelete: (id: string) => void;
}

export default function ExtraMenuItemEditor(props: ExtraMenuItemEditorProps) {

    const theme = useTheme();

    const [id, setId] = useState(props?.data?.id);
    const [name, setName] = useState(props?.data?.name || "");
    // const [description, setDescription] = useState(props?.extraMenuitem?.description || "");
    const [weight, setWeight] = useState(parseWeight(props?.data?.weight));
    const [price, setPrice] = useState(props?.data?.price || 0);
    const [weightUnit, setWeightUnit] = useState(parseWeightUnit(props?.data?.weight) || "g");
    const [existingExtraMenuitems, setExistingExtraMenuitems] = useState<ExtraMenuitem[]>([]);
    const [selectedExitingExtraMenuitem, setSelectedExitingExtraMenuitem] = useState<string>('');
    const [pristine, setPristine] = useState<boolean>(true);

    const axiosSecure = useAxiosSecure();
    const auth = useSelector((state: RootState) => state.auth);


    useEffect(() => {
        apiCall();
    }, [])


    useEffect(() => {
        setId(props?.data?.id);
        setName(props?.data?.name || "");
        setWeight(parseWeight(props?.data?.weight));
        setPrice(props?.data?.price || 0);
        setWeightUnit(parseWeightUnit(props?.data?.weight) || "g");
        apiCall();
    }, [props?.data])


    useEffect(() => {

        // if there was an change, chreate new ID
        props?.onChangeData({
            id: props?.action === 'UPDATE' ? id : (pristine ? id : newGuid()),
            name,
            description: "",
            weight: `${weight}${weightUnit}`,
            price,
            viewOrder: props?.data?.viewOrder || 0,
            isHidden: false,
            _isExisting: props?.action === 'UPDATE' ? true : pristine
        });

    }, [name, weight, weightUnit, price])


    async function apiCall() {

        let filter = "";

        // TODO
        // if (auth.user) {
        //     filter += `restaurationId=${auth.user?.restaurationId}&`;
        // }

        let response = await axiosSecure.get(`/api/extra_menuitem?${filter}`, false);

        let extistingIds = props?.extraCategory?.extraCategoryExtraMenuitems.map(e => e.extraMenuitemId);

        setExistingExtraMenuitems(response.data?.filter((m: ExtraMenuitem) => extistingIds?.indexOf(m.id) === -1));
    }

    const textFieldStyle = {
        background: 'white'
    }

    function handleNameChange(event: any) {
        setName(event.target.value);
        setPristine(false);
    }

    function handlePriceChange(event: any) {
        setPrice(event.target.value);
        setPristine(false);
    }

    function handleWeightChange(event: any) {
        setWeight(event.target.value);
        setPristine(false);
    }

    function handleWeightUnitChange(event: any) {
        setWeightUnit(event.target.value);
        setPristine(false);
    }

    function parseWeight(weight: string | undefined) {
        var res = weight?.replace(/[^\d.-]/g, "");
        return res || "";
    }

    function parseWeightUnit(weight: string | undefined) {
        var res = weight?.replace(/[\W\d]/g, "");
        return res || "";
    }

    function handleExistingExtraMenuitemChange(event: SelectChangeEvent) {
        setSelectedExitingExtraMenuitem(event.target.value as string);

        if (event.target.value) {

            const extraMenuitem = existingExtraMenuitems.find(e => e.id === event.target.value) as ExtraMenuitem;

            setId(extraMenuitem.id)
            setName(extraMenuitem.name);
            setPrice(extraMenuitem.price);
            setWeight(parseWeight(extraMenuitem.weight));
            setWeightUnit(parseWeightUnit(extraMenuitem.weight) || "g");
            setPristine(true);


            props?.onChangeData({
                id: extraMenuitem?.id,
                name: extraMenuitem?.name,
                description: "",
                weight: extraMenuitem?.weight,
                price: extraMenuitem?.price,
                viewOrder: 0,
                isHidden: false,
                _isExisting: props?.action === 'UPDATE' ? true : pristine
            });

        } else {
            setId(props?.data?.id);
            setName("");
            setPrice(0);
            setWeight(parseWeight(""));
            setWeightUnit("g");
            setPristine(true);
        }
    }

    function handleDeleteExtraMenuitem() {
        props?.onDelete(props?.data?.id);
    }

    return (

        <Box sx={{
            width: '100%',
            marginTop: '20px'
        }}>
            <Stack spacing={2}>

                {props?.action === 'CREATE' && <>
                    <FormControl fullWidth>
                        <InputLabel>Existujúca modifikácia</InputLabel>
                        <Select
                            value={selectedExitingExtraMenuitem}
                            onChange={handleExistingExtraMenuitemChange}
                        >
                            <MenuItem key={''} value={''}>
                                Vytvoriť novú
                            </MenuItem>

                            {existingExtraMenuitems.map((extraMenuitem, index) => (
                                <MenuItem key={extraMenuitem.id} value={extraMenuitem.id}>
                                    {extraMenuitem.name} ({extraMenuitem.weight}) - {extraMenuitem.price}€
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </>}

                {/* {selectedExitingExtraMenuitem === '' &&  */}
                <>
                    <TextField
                        sx={textFieldStyle}
                        label="Názov"
                        variant="outlined"
                        value={name}
                        onChange={handleNameChange}
                    />

                    <RadioGroup
                        row
                        value={weightUnit}
                        onChange={handleWeightUnitChange}
                    >
                        <Stack direction="row" spacing={2}>
                            <TextField
                                sx={textFieldStyle}
                                type="number"
                                label="Veľkosť"
                                value={weight}
                                variant="outlined"
                                onChange={handleWeightChange}
                            />
                            <div>
                                <FormControlLabel value="g" control={<Radio />} label="gramov" />
                                <FormControlLabel value="ml" control={<Radio />} label="mililitrov" />
                            </div>
                        </Stack>
                    </RadioGroup>

                    <TextField
                        sx={textFieldStyle}
                        type="number"
                        label="Cena v EUR"
                        variant="outlined"
                        value={price}
                        onChange={handlePriceChange}
                    />
                </>
                {/* } */}


                {props?.action === 'UPDATE' && <>
                    <Divider />

                    <Button
                        color="black"
                        variant="text"
                        onClick={handleDeleteExtraMenuitem}
                    >
                        Natrvalo odstrániť modifikáciu
                    </Button>
                </>}

            </Stack>
        </Box>
    );
}