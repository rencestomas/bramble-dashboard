import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export interface AlertDialogProps {
    open: boolean;
    okLabel: string;
    title: string;
    content: string;
    onClose: () => void;
}

export default function AlertDialog(props: AlertDialogProps) {

    const { onClose, open } = props;

    const handleOkClose = () => {
        onClose();
    };

    return (
        <Dialog
            open={open}
            TransitionComponent={Transition}
        >
            <DialogTitle>{props?.title}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {props?.content}
                </DialogContentText>
            </DialogContent>
            <DialogActions
                sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <Button variant='contained' size="small" onClick={handleOkClose}>{props?.okLabel || 'Ok'}</Button>
            </DialogActions>
        </Dialog>
    );
}