import { styled, useTheme, Theme, CSSObject } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MuiDrawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import PlayCircleIcon from '@mui/icons-material/PlayCircleFilled';
import HeadsetMicIcon from '@mui/icons-material/HeadsetMic';
import LoginIcon from '@mui/icons-material/Login';
import SideMenuItem from '../SideMenuItem/SideMenuItem';
import { HasPermission } from '../../core/security/HasPermission/HasPermission';
import { SecurityRole } from '../../core/security/SecurityRole';
import { IsLoggedIn } from '../../core/security/IsLoggedIn/IsLoggedIn';
import { useDispatch, useSelector } from 'react-redux';
import { IsNotLoggedIn } from '../../core/security/IsNotLoggedIn/IsNotLoggedIn';
import { RootState } from '../../core/store/store';
import { useLocation } from 'react-router-dom';
import { Stack } from '@mui/material';
import { IsRestaurationOpen } from '../../core/security/IsRestaurationOpen/IsRestaurationOpen';
import Button from '@mui/material/Button';
import CancelIcon from '@mui/icons-material/Cancel';
import RoomServiceOutlinedIcon from '@mui/icons-material/RoomServiceOutlined';
import RestoreIcon from '@mui/icons-material/Restore';
import ImportContactsIcon from '@mui/icons-material/ImportContacts';
import EjectOutlinedIcon from '@mui/icons-material/EjectOutlined';
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import RedeemIcon from '@mui/icons-material/Redeem';
import BusinessCenterOutlinedIcon from '@mui/icons-material/BusinessCenterOutlined';
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';
import useAxiosSecure from '../../../hooks/useAxiosSecure';
import { useEffect, useState } from 'react';
import SidePanel from '../SidePanel/SidePanel';
import MenuNavBar from '../MenuNavBar/MenuNavBar';
import LanguageSelect from '../LanguageSelect/LanguageSelect';
import HelpOutlineOutlinedIcon from '@mui/icons-material/HelpOutlineOutlined';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import { useTranslation } from 'react-multi-lang';
import { updateAuth, updateUserRestauration } from '../../core/store/features/auth/authSlice';
import { setPanelOpen } from '../../core/store/features/sidePanel/sidePanelSlice';


const drawerWidth = 240;

const shadowMixin = (theme: Theme): CSSObject => ({
    boxShadow: '0px 8px 9px -5px rgb(0 0 0 / 20%), 0px 15px 22px 2px rgb(0 0 0 / 14%), 0px 6px 28px 5px rgb(0 0 0 / 12%);',
});

const openedMixin = (theme: Theme): CSSObject => ({
    width: drawerWidth,
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
    overflow: 'hidden',
});

const closedMixin = (theme: Theme): CSSObject => ({
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    overflow: 'hidden',
    width: `calc(${theme.spacing(11)} + 1px)`
});

const MenuDrawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        boxSizing: 'border-box',
        ...(open && {
            ...openedMixin(theme),
            '& .MuiDrawer-paper': { ...openedMixin(theme), ...shadowMixin(theme) },
        }),
        ...(!open && {
            ...closedMixin(theme),
            '& .MuiDrawer-paper': closedMixin(theme),
        }),
    }),
);

const SidePanelDrawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        boxSizing: 'border-box',
        overflow: 'scroll',
        '& .MuiPaper-root': {
            top: 60,
            height: 'fill-available',
            overflow: 'auto !important'
        },
        ...(open && {
            ...openedMixin(theme),
            '& .MuiDrawer-paper': openedMixin(theme),
        }),
        ...(!open && {
            ...closedMixin(theme),
            '& .MuiDrawer-paper': closedMixin(theme),
        }),
    }),
);

const NavigationItem = styled(Stack)(({ theme }) => ({
    fontSize: 'x-large',
    fontWeight: '600',
    color: '#AEAEB2'
}));


const NavigationList = styled(List)(({ theme }) => ({
    '&& .Mui-selected, && .Mui-selected:hover': {
        //background: 'linear-gradient(180deg, #FFEEF1 0.64%, #FFC6CF 102.46%)',
        background: '#FBEAEA',
        borderRadius: '10px',
        color: theme.palette.primary.main,

        '& .MuiListItemIcon-root': {
            color: theme.palette.primary.main
        },
    },
    // hover states
    '& .MuiListItemButton-root:hover': {
        background: '#FDF7F7',
        color: theme.palette.primary.main,

        '& .MuiListItemIcon-root': {
            color: theme.palette.primary.main
        },
    },
    '& .MuiListItemButton-root': {
        color: '#AEAEB2',

        '& .MuiListItemIcon-root': {
            color: '#AEAEB2'
        }
    }
}));

export default function Layout(props: any) {

    const [open, setOpen] = useState(false);
    const [isMobile, setIsMobile] = useState<boolean>(window.innerWidth <= 768);

    const currentUser = useSelector((state: RootState) => state?.auth?.user);
    const auth = useSelector((state: RootState) => state.auth);
    const sidePanel = useSelector((state: RootState) => state.sidePanel);

    const location = useLocation();
    const dispatch = useDispatch();
    const axiosSecure = useAxiosSecure();
    const theme = useTheme();
    const t = useTranslation();

    const handleMouseOver = () => {
        if (!isMobile) {
            setOpen(true);
        }
    };

    const handleMouseOut = () => {
        setOpen(false)
    };

    function handleWindowSizeChange() {
        setIsMobile(window.innerWidth <= 768);
    }

    function handleLogOut() {
        dispatch(updateAuth({
            accessToken: null,
            refreshToken: null,
            user: null
        }));

        dispatch(setPanelOpen(false));
    }

    async function handleRestaurationClose() {

        let _restauration = ({ ...auth.restauration });
        _restauration!.isOpen = false;

        let response = await axiosSecure.put(`/api/restauration`,
            {
                id: _restauration.id,
                isOpen: false
            }, true);

        dispatch(updateUserRestauration({
            restauration: _restauration as any
        }));
    }

    useEffect(() => {
        window.addEventListener('resize', handleWindowSizeChange);
        return () => {
            window.removeEventListener('resize', handleWindowSizeChange);
        }
    }, []);


    function renderNavigation(path: string) {
        switch (path) {
            case '/overview':
                return (<NavigationItem direction="row" justifyContent="start" spacing={1}><RoomServiceOutlinedIcon /> <Typography><strong>{t('Orders')}</strong></Typography></NavigationItem>);
            case '/overview/orders':
                return (<NavigationItem direction="row" justifyContent="start" spacing={1}><RoomServiceOutlinedIcon /> <Typography><strong>{t('Orders')}</strong></Typography></NavigationItem>);
            case '/history':
                return (<NavigationItem direction="row" justifyContent="start" spacing={1}><RestoreIcon /> <Typography><strong>{t('History')}</strong></Typography></NavigationItem>);
            // case '/daily-menu':
            //     return (<Stack direction="row" justifyContent="start" spacing={1} ><TodayIcon /> <Typography><strong>{t('DailyMenu')}</strong></Typography></Stack>);
            case '/standing-offer':
                return (<NavigationItem direction="row" justifyContent="start" spacing={1}><ImportContactsIcon /> <Typography><strong>{t('StandingOffer')}</strong></Typography></NavigationItem>);
            case '/modifications':
                return (<NavigationItem direction="row" justifyContent="start" spacing={1}><EjectOutlinedIcon /> <Typography><strong>{t('Modifications')}</strong></Typography></NavigationItem>);
            case '/manage':
                return (<NavigationItem direction="row" justifyContent="start" spacing={1}><RedeemIcon /> <Typography><strong>{t('Manage')}</strong></Typography></NavigationItem>);
            case '/admin':
                return (<NavigationItem direction="row" justifyContent="start" spacing={1}><AdminPanelSettingsIcon /> <Typography><strong>{t('Admin')}</strong></Typography></NavigationItem>);
            // case '/sales':
            //     return (<NavigationItem direction="row" justifyContent="start" spacing={1}><BusinessCenterOutlinedIcon /> <Typography><strong>{t('Sales')}</strong></Typography></NavigationItem>);
            case '/help':
                return (<NavigationItem direction="row" justifyContent="start" spacing={1}><HeadsetMicIcon /> <Typography><strong>{t('Help')}</strong></Typography></NavigationItem>);
            default:
                return (<NavigationItem direction="row" justifyContent="start" spacing={1}><PlayCircleIcon /> <Typography><strong>{t('Navigation')}</strong></Typography></NavigationItem>);
        }
    }

    function handleSidePanel(open: boolean) {
        dispatch(setPanelOpen(open));
    }

    function getWidth() {
        if (isMobile) return '100%';
        if ([/*'/daily-menu',*/ '/standing-offer', '/modifications', '/overview/orders'].indexOf(location?.pathname) > -1) return '50%';
        return '100%';
    }

    return (
        <Box sx={{
            display: 'flex',
            flexDirection: 'column',
            minHeight: '100vh'
        }}>
            <CssBaseline />

            <IsLoggedIn>
                <MenuDrawer
                    variant="permanent"
                    open={open}
                    onMouseOver={handleMouseOver}
                    onMouseOut={handleMouseOut}
                >
                    <Box
                        sx={{
                            padding: '0px 10px',
                            overflowY: open ? 'auto' : 'hidden',
                            overflowX: 'hidden'
                        }}
                    >
                        <Typography sx={{
                            opacity: open ? 1 : 0,
                            margin: '5px 25px',
                            fontSize: 'xx-large',
                            fontWeight: 'bold',
                            color: theme.palette.primary.main
                        }}>
                            Bramble
                        </Typography>

                        <IsNotLoggedIn>
                            <NavigationList>

                                <SideMenuItem text={t('Login')} link='/login' open={open}><LoginIcon /></SideMenuItem>

                            </NavigationList>
                        </IsNotLoggedIn>

                        <HasPermission allowedRoles={[SecurityRole.Manager, SecurityRole.Kitchen, SecurityRole.Potman]}>
                            <NavigationList>

                                <SideMenuItem text={t('Orders')} link='/overview' open={open}><RoomServiceOutlinedIcon /></SideMenuItem>
                                <SideMenuItem text={t('History')} link='/history' open={open}><RestoreIcon /></SideMenuItem>

                            </NavigationList>
                        </HasPermission>

                        <HasPermission allowedRoles={[SecurityRole.Manager]}>
                            <Divider />
                            <NavigationList>

                                {/* <SideMenuItem text={t('DailyMenu')} link='/daily-menu' open={open}><TodayIcon /></SideMenuItem> */}
                                <SideMenuItem text={t('StandingOffer')} link='/standing-offer' open={open}><ImportContactsIcon /></SideMenuItem>
                                <SideMenuItem text={t('Modifications')} link='/modifications' open={open}><EjectOutlinedIcon /></SideMenuItem>

                            </NavigationList>
                        </HasPermission>


                        <HasPermission allowedRoles={[SecurityRole.Manager]}>
                            <Divider />
                            <NavigationList>
                                <SideMenuItem text={t('Manage')} link='/manage' open={open}><BusinessCenterOutlinedIcon /></SideMenuItem>
                            </NavigationList>
                        </HasPermission>

                        <HasPermission allowedRoles={[SecurityRole.Manager]}>
                            <NavigationList>
                                <SideMenuItem text={t('Stripe Connect')} link='/stripe-connect' open={open}><MonetizationOnIcon /></SideMenuItem>
                            </NavigationList>
                        </HasPermission>


                        <HasPermission allowedRoles={[SecurityRole.Admin]}>
                            <NavigationList>
                                <SideMenuItem text={t('Admin')} link='/admin' open={open}><AdminPanelSettingsIcon /></SideMenuItem>
                            </NavigationList>
                        </HasPermission>
                    </Box>

                    <NavigationList
                        sx={{
                            marginTop: 'auto',
                            bottom: '0',
                            padding: '10px',
                            background: 'white'
                        }}>

                        <LanguageSelect
                            text={t('Language')}
                            open={open}
                            onMouseOver={handleMouseOver}
                            onMouseOut={handleMouseOut}
                        />

                        <SideMenuItem text={t('Help')} link='#' open={open}><HelpOutlineOutlinedIcon /></SideMenuItem>
                        <SideMenuItem text={t('Tutorials')} link='#' open={open}><InfoOutlinedIcon /></SideMenuItem>

                        <SideMenuItem text={currentUser?.login || ""} link='/login' open={open} onClick={handleLogOut}><AccountCircleIcon /></SideMenuItem>
                    </NavigationList>
                </MenuDrawer>
            </IsLoggedIn>

            <IsLoggedIn>
                {/* // TODO: make header component */}
                <Stack
                    sx={{
                        zIndex: 1002,
                        height: '60px',
                        padding: `0px 10px 0px calc(${theme.spacing(13)} + 1px)`,
                        borderBottom: '1px solid #E0E0E0'
                    }}
                    direction="row"
                    alignItems="center"
                    justifyContent="space-between"
                    spacing={2}
                >
                    {renderNavigation(location?.pathname)}

                    <HasPermission allowedRoles={[SecurityRole.Manager, SecurityRole.Kitchen]}>
                        <IsRestaurationOpen>
                            <Button
                                color="primary"
                                variant="outlined"
                                onClick={handleRestaurationClose}
                                startIcon={<CancelIcon />}
                            >
                                {t('CloseRestauration')}
                            </Button>
                        </IsRestaurationOpen>
                    </HasPermission>
                </Stack>

                {/* TODO */}
                {[/*'/daily-menu',*/ '/standing-offer', '/modifications'].indexOf(location?.pathname) > -1 &&
                    <Box
                        sx={{
                            width: getWidth()
                        }}
                    >
                        {/* <Divider /> */}
                        <MenuNavBar />
                    </Box>
                }

                {/* <Typography sx={{
                    fontSize: 'x-large',
                    fontWeight: 'bold',
                    margin: '20px 5px',
                    color: theme.palette.primary.main
                }}> */}

                {/* <Divider /> */}
            </IsLoggedIn>

            <Box component="main"
                sx={{
                    flexGrow: 1,
                    bgcolor: theme.palette.backgroundColor.main,
                    borderRight: '1px solid #E0E0E0',
                    width: getWidth(),
                    overflowY: 'auto',
                    overflowX: 'hidden',
                    height: 0,
                    padding: currentUser != null ? `24px 24px 24px calc(${theme.spacing(10)} + 1px)` : '96px 24px 24px 24px'
                }}>
                {/* <Box sx={{
                    fontSize: 'xx-large',
                    margin: '20px',
                    height: '300px'
                }}> */}
                <Box sx={{ margin: '20px' }}>{props.children}</Box>
                {/* </Box> */}

                {isMobile && <MuiDrawer
                    open={sidePanel.isOpen}
                    variant="temporary"
                    anchor="bottom"
                    onClose={() => handleSidePanel(false)}
                >
                    <SidePanel />
                </MuiDrawer>}

                {!isMobile && <SidePanelDrawer
                    sx={{
                        width: sidePanel.isOpen ? '50%' : '0%',
                        flexShrink: 0,
                        '& .MuiDrawer-paper': {
                            width: sidePanel.isOpen ? '50%' : '0%',
                        },
                    }}
                    open={sidePanel.isOpen}
                    variant="permanent"
                    anchor="right"
                >
                    <SidePanel />
                </SidePanelDrawer>}
            </Box>

        </Box>
    );
}