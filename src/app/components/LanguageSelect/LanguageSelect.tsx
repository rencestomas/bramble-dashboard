import Box from '@mui/material/Box';
import { countries } from './countries';
import { ListItem, ListItemButton, ListItemText, MenuItem, Select, SelectChangeEvent } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../core/store/store';
import { setLanguage } from '../../core/store/features/ui/uiSlice';


const selectStyle = {
    padding: '0px 1px',
    boxShadow: 'none',
    '.MuiOutlinedInput-input': {
        padding: 'unset !important'
    },
    '.MuiOutlinedInput-notchedOutline': {
        border: 0
    }
}

export interface LanguageSelectProps {
    text: string;
    open: boolean;
    onMouseOver: (value: boolean) => void;
    onMouseOut: (value: boolean) => void;
    onChange?: (value: string) => void;
}

export default function LanguageSelect(props: LanguageSelectProps) {

    const language = useSelector((state: RootState) => state.ui.language);
    const dispatch = useDispatch();

    const handleChange = (event: SelectChangeEvent) => {
        props?.onChange?.(event.target.value as string);
        dispatch(setLanguage(event.target.value as string));
    };

    const handleMouseOver = () => {
        props?.onMouseOver(true)
    };

    const handleMouseOut = () => {
        props?.onMouseOut(false)
    };

    return (
        <>
            <ListItem
                key={props.text}
                // onClick={props.onClick}
                sx={{
                    padding: '4px 10px'
                }}
            >
                <ListItemButton
                    sx={{
                        height: 48,
                        padding: '4px 12px',
                        borderRadius: '10px',
                    }}
                >
                    <Select
                        value={language}
                        onChange={handleChange}
                        onMouseOver={handleMouseOver}
                        onMouseOut={handleMouseOut}
                        inputProps={{ IconComponent: () => null }}
                        sx={{
                            mr: props.open ? 2 : 'auto',
                            ...selectStyle
                        }}
                        renderValue={(country) => (
                            <img
                                loading="lazy"
                                width="20"
                                src={`https://flagcdn.com/w20/${country.toLowerCase()}.png`}
                                srcSet={`https://flagcdn.com/w40/${country.toLowerCase()}.png 2x`}
                                alt=""
                            />
                        )}
                    >
                        {countries.map((country, index) => (
                            <MenuItem key={country.code} value={country.code}>
                                <Box sx={{ '& > img': { mr: 2, flexShrink: 0 } }}>
                                    <img
                                        loading="lazy"
                                        width="20"
                                        src={`https://flagcdn.com/w20/${country.code.toLowerCase()}.png`}
                                        srcSet={`https://flagcdn.com/w40/${country.code.toLowerCase()}.png 2x`}
                                        alt=""
                                    />
                                    {country.label}
                                </Box>
                            </MenuItem>
                        ))}
                    </Select>

                    <ListItemText primary={props.text}
                        primaryTypographyProps={{
                            fontWeight: '600',
                            fontSize: "1.0rem"
                        }}
                        sx={{
                            opacity: props.open ? 1 : 0
                        }} />
                </ListItemButton>
            </ListItem>
        </>
    );
}