import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import { Restauration } from '../../shared/interfaces/restauration.interface';
import { FormControl, Grid, InputLabel, ListItemText, MenuItem, Select, SelectChangeEvent, TextField, Typography, styled } from '@mui/material';
import { useTranslation } from 'react-multi-lang';
import { useEffect, useState } from 'react';
import { newGuid } from '../../core/utilities';


const StyledTextField = styled(TextField)(({ theme }) => ({
    background: 'white',
    width: '100%'
}));

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export interface AddRestaurantDialogProps {
    open: boolean;
    yesLabel: string;
    noLabel: string;
    festivalId: string;
    id?: string;
    restaurantId?: string;
    location?: string;
    restaurants: Restauration[];
    onClose: (e: any) => void;
}

export default function AddRestaurantDialog(props: AddRestaurantDialogProps) {

    const { onClose, open } = props;
    const [id, setId] = useState("");
    const [location, setLocation] = useState("");
    const [selectedRestaurantId, setSelectedRestaurantId] = useState("");


    const t = useTranslation();

    useEffect(() => {
        // update data
        setId(props?.id || "");
        setLocation(props?.location || "");
        setSelectedRestaurantId(props?.restaurantId || "");
    }, [props])

    const handleOkClose = () => {
        onClose({
            id: props?.id ? id : newGuid(),
            festivalId: props?.festivalId,
            restaurantId: selectedRestaurantId,
            location: location
        });
    };

    const handleCancelClose = () => {
        onClose(null);
    };

    function handleLocationChange(event: any) {
        setLocation(event.target.value);
    }

    function handleSelectedRestaurantChange(event: SelectChangeEvent) {
        setSelectedRestaurantId(event.target.value as string);
    }

    return (
        <Dialog
            open={open}
            TransitionComponent={Transition}
        >
            <DialogTitle>{props?.id ? t('UpdateRestaurantLocation') : t('AddRestaurantLocation')}</DialogTitle>
            <DialogContent>
                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: '400px',
                        height: '150px'
                    }}
                >
                    <Grid item xs={12}>
                        <FormControl disabled={props?.id ? true : false} size="small" sx={{ width: '100%' }}>
                            <InputLabel>{t('Restaurant')}</InputLabel>
                            <Select
                                value={selectedRestaurantId}
                                label={t('Restaurant')}
                                onChange={handleSelectedRestaurantChange}
                            >
                                {props?.restaurants?.map((restaurant) => (
                                    <MenuItem key={restaurant.id} value={restaurant.id}>
                                        <ListItemText primary={restaurant.name} />
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>

                    <Grid item xs={12}>
                        <Typography><strong>{t('Location')}</strong></Typography>
                        <StyledTextField
                            variant="outlined"
                            value={location}
                            onChange={handleLocationChange}
                        />
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button color="black" variant='contained' size="small" onClick={handleCancelClose}>{props?.noLabel || 'Cancel'}</Button>
                <Button variant='contained' size="small" onClick={handleOkClose}>{props?.yesLabel || 'Ok'}</Button>
            </DialogActions>
        </Dialog>
    );
}