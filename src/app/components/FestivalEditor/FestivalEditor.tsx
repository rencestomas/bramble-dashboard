import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from "react";
import { Button, Chip, Divider, Grid, IconButton, ImageList, ImageListItem, ImageListItemBar, ListSubheader, Select, Stack, styled, TextField, useTheme } from "@mui/material";
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import DeleteIcon from '@mui/icons-material/Delete';
import FileUploading from 'react-files-uploading';
import { Menuitem } from '../../shared/interfaces/menuitem.interface';
import useAxiosSecure from '../../../hooks/useAxiosSecure';
import { useSelector } from 'react-redux';
import { RootState } from '../../core/store/store';
import { ExtraCategory } from '../../shared/interfaces/extra-category.interface';
import { useTranslation } from 'react-multi-lang';
import { MINIO_URL } from '../../core/api/axios';
import { Restauration } from '../../shared/interfaces/restauration.interface';
import { Festival } from '../../shared/interfaces/festival.interface';
import MultiSelect from '../MultiSelect/MultiSelect';
import AddRestaurantDialog from '../AddRestaurantDialog/AddRestaurantDialog';
import FestivalRestaurantItem from '../FestivalRestaurantItem/FestivalRestaurantItem';
import ConfirmDialog from '../ConfirmDialog/ConfirmDialog';


const StyledTextField = styled(TextField)(({ theme }) => ({
    background: 'white',
    width: '100%'
}));

export interface FestivalEditorProps {
    action: 'CREATE' | 'UPDATE' | 'VIEW' | null,
    data: Festival;
    onChangeData: (data: any) => void;
    onDelete: (e: Festival) => void;
}

export default function FestivalEditor(props: FestivalEditorProps) {

    const theme = useTheme();

    const [name, setName] = useState(props?.data?.name || "");
    const [description, setDescription] = useState(props?.data?.description || "");
    const [siteUrl, setSiteUrl] = useState(props?.data?.siteUrl || "");
    const [street, setStreet] = useState(props?.data?.street || "");
    const [zipCode, setZipCode] = useState(props?.data?.zipCode || "");
    const [city, setCity] = useState(props?.data?.city || "");
    const [state, setState] = useState(props?.data?.state || "");
    const [fileToUpload, setFileToUpload] = useState<File | null>(null);
    const [thumbnail, setThumbnail] = useState<string>("../../../../src/assets/festival_image_placeholder.png");
    const [festivalRestaurants, setFestivalRestaurants] = useState<any[]>([]);
    const [openAddRestaurant, setOpenAddRestaurant] = useState(false);
    const [openDeleteFestivalRestaurantConfirm, setOpenDeleteFestivalRestaurantConfirm] = useState(false);
    const [toDeleteItem, setToDeleteItem] = useState<any>(null);
    const [editFestivalRestaurant, setEditFestivalRestaurant] = useState<any>(null);


    const axiosSecure = useAxiosSecure();
    const auth = useSelector((state: RootState) => state.auth);
    const restaurants = useSelector((state: RootState) => [...state.restaurant.restaurants]);
    const t = useTranslation();


    useEffect(() => {
        async function apiCall() {

            let filter = "";

            if (auth.user) {
                filter += `restaurationId=${auth.user?.restaurationId}&`;
            }

        }

        apiCall();
    }, [])


    useEffect(() => {

        setName(props?.data?.name || "");
        setDescription(props?.data?.description || "");
        setSiteUrl(props?.data?.siteUrl || "");
        setStreet(props?.data?.street || "");
        setZipCode(props?.data?.zipCode || "");
        setCity(props?.data?.city || "");
        setState(props?.data?.state || "");

        setFileToUpload(null);
        setThumbnail(`${MINIO_URL}/festivals/${props?.data?.id}.jpg`);

        setFestivalRestaurants(props?.data?.festivalRestaurants || []);

    }, [props?.data])


    useEffect(() => {

        props?.onChangeData({
            id: props?.data?.id,
            name,
            description,
            siteUrl,
            street,
            zipCode,
            city,
            state,
            image: fileToUpload ? fileToUpload : null,
            festivalRestaurants
        });

    }, [name, description, siteUrl, street, zipCode, city, state, fileToUpload, festivalRestaurants])


    function handleNameChange(event: any) {
        setName(event.target.value);
    }

    function handleDescriptionChange(event: any) {
        setDescription(event.target.value);
    }

    function handleSiteUrlChange(event: any) {
        setSiteUrl(event.target.value);
    }

    function handleStreetChange(event: any) {
        setStreet(event.target.value);
    }

    function handleZipCodeChange(event: any) {
        setZipCode(event.target.value);
    }

    function handleCityChange(event: any) {
        setCity(event.target.value);
    }

    function handleStateChange(event: any) {
        setState(event.target.value);
    }

    function handleDeleteFestival() {
        props?.onDelete(props?.data);
    }

    function handleFestivalRestaurantDelete(e: any) {
        setOpenDeleteFestivalRestaurantConfirm(true);
        setToDeleteItem(e);
    }

    function handleFestivalRestaurantClick(e: any) {
        setEditFestivalRestaurant(e);
        setOpenAddRestaurant(true);
    }

    function handleOpenAddRestaurant() {
        setOpenAddRestaurant(true);
    }

    function handleFileChange(fileList: File[]) {
        setFileToUpload(fileList[0]);
        const objectUrl = URL.createObjectURL(fileList[0]);
        setThumbnail(objectUrl);
        // setPreview(objectUrl)

        // free memory when ever this component is unmounted
        // return () => URL.revokeObjectURL(objectUrl)
    };

    function handleRemoveFestivalThumbnailClick(event: any) {
        event.stopPropagation();
        setFileToUpload(null);
        setThumbnail('');
    }

    function handleDeleteFestivalRestaurantConfirmClose(value: any) {
        if (value) {
            let _newFestivalRestaurants = festivalRestaurants.filter(
                fr => !(fr.festivalId === toDeleteItem.festivalId && fr.restaurantId === toDeleteItem.restaurantId)
            );
            setFestivalRestaurants([..._newFestivalRestaurants]);
        }

        setOpenDeleteFestivalRestaurantConfirm(false);
        setToDeleteItem(null);
    }

    function handleAddRestaurantClose(value: any) {
        setOpenAddRestaurant(false);
        setEditFestivalRestaurant(null);
        if (value != null) {
            if (festivalRestaurants.find(fr => fr.id === value?.id)) {

                let newFestivalRestaurants = festivalRestaurants.map(fr => {
                    if (fr.id === value.id) {
                        return ({ ...fr, ...value });
                    } else {
                        return fr;
                    }
                });
                setFestivalRestaurants([...newFestivalRestaurants]);
            } else {
                setFestivalRestaurants([...festivalRestaurants, ({
                    ...value,
                    restaurantName: restaurants.find(r => r.id === value.restaurantId)?.name
                })]);
            }
        }
    };


    return (
        <>
            <AddRestaurantDialog
                open={openAddRestaurant}
                onClose={handleAddRestaurantClose}
                restaurants={restaurants}
                festivalId={props.data.id}
                id={editFestivalRestaurant?.id}
                restaurantId={editFestivalRestaurant?.restaurantId}
                location={editFestivalRestaurant?.location}
                noLabel={t('CancelAction')}
                yesLabel={t('SaveAction')}
            />

            <ConfirmDialog
                open={openDeleteFestivalRestaurantConfirm}
                onClose={handleDeleteFestivalRestaurantConfirmClose}
                title={t('RemoveRestaurantFromFestival')}
                content={t('RemoveRestaurantFromFestivalMessage')}
                noLabel={t('KeepAction')}
                yesLabel={t('RemoveRestaurantInFestival')}
            />

            <Stack spacing={2}>
                <Grid container spacing={2}>

                    <Grid item xs={12}>
                        <Typography><strong>{t('Title')}</strong></Typography>
                        <StyledTextField
                            variant="outlined"
                            value={name}
                            onChange={handleNameChange}
                        />
                    </Grid>

                    <Grid item xs={8}>
                        <Typography><strong>{t('Description')}</strong></Typography>
                        <StyledTextField
                            variant="outlined"
                            multiline
                            rows={4}
                            value={description}
                            onChange={handleDescriptionChange}
                        />
                    </Grid>

                    <Grid item xs={4}>
                        <Typography><strong>{t('Image')}</strong></Typography>
                        <FileUploading value={fileToUpload != null ? [fileToUpload] : []} onChange={handleFileChange}
                            acceptType={['jpg', 'jpeg', 'png']} maxFileSize={2097152}>
                            {({
                                fileList,
                                errors,
                                isDragging,
                                onFileUpload,
                                onFileRemoveAll,
                                onFileUpdate,
                                onFileRemove,
                                dragProps,
                            }) => {
                                return (
                                    <>
                                        <Box
                                            id="btn-upload"
                                            onClick={onFileUpload}
                                            {...dragProps}
                                            sx={{
                                                background: 'white',
                                                position: 'relative',
                                                color: '#AEAEB2', // theme.palette.primary.main,
                                                borderRadius: '8px',
                                                border: `3px dashed #AEAEB2`, // ${theme.palette.primary.main}`,
                                                height: '125px',
                                                textAlign: 'center',
                                                cursor: 'pointer',
                                                transition: 'filter ease-in-out 200ms',
                                                ':hover': {
                                                    filter: 'brightness(0.85)'
                                                }
                                            }}
                                        >
                                            {errors && errors.maxNumber && (
                                                <span>Number of selected files exceed maxNumber</span>
                                            )}

                                            <>
                                                <Stack
                                                    spacing={2}
                                                    direction="column"
                                                    justifyContent="space-between"
                                                    sx={{
                                                        position: 'absolute',
                                                        zIndex: 1,
                                                        color: 'white',
                                                        left: '50%',
                                                        top: '50%',
                                                        transform: 'translate(-50%, -50%)'
                                                    }}
                                                >
                                                    <Typography
                                                        sx={{
                                                            textOverflow: 'ellipsis',
                                                            overflow: 'hidden',
                                                            whiteSpace: 'nowrap'
                                                        }}
                                                    >
                                                        Click here or drop
                                                    </Typography>

                                                    {fileToUpload != null && (
                                                        <Stack
                                                            spacing={2}
                                                            direction="row"
                                                            justifyContent="space-between"
                                                        >
                                                            <Typography
                                                                sx={{
                                                                    textOverflow: 'ellipsis',
                                                                    overflow: 'hidden',
                                                                    whiteSpace: 'nowrap',
                                                                    maxWidth: '100px'
                                                                }}
                                                            >
                                                                {fileToUpload?.name}

                                                            </Typography>

                                                            <DeleteIcon
                                                                onClick={(e) => handleRemoveFestivalThumbnailClick(e)} />
                                                        </Stack>)}
                                                </Stack>
                                                <img
                                                    style={{
                                                        filter: "brightness(70%)",
                                                        borderRadius: "5px",
                                                        height: "100%",
                                                        width: "100%",
                                                        objectFit: "cover"
                                                    }}
                                                    src={thumbnail}
                                                    onError={({ currentTarget }) => {
                                                        currentTarget.onerror = null; // prevents looping
                                                        currentTarget.src = require("../../../../src/assets/festival_image_placeholder.png");
                                                    }}
                                                />

                                            </>

                                        </Box>
                                    </>
                                );
                            }}
                        </FileUploading>
                    </Grid>

                    <Grid item xs={12}>
                        <Typography><strong>{t('SiteUrl')}</strong></Typography>
                        <StyledTextField
                            variant="outlined"
                            value={siteUrl}
                            onChange={handleSiteUrlChange}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <Typography><strong>{t('Street')}</strong></Typography>
                        <StyledTextField
                            variant="outlined"
                            value={street}
                            onChange={handleStreetChange}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <Typography><strong>{t('ZipCode')}</strong></Typography>
                        <StyledTextField
                            variant="outlined"
                            value={zipCode}
                            onChange={handleZipCodeChange}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <Typography><strong>{t('City')}</strong></Typography>
                        <StyledTextField
                            variant="outlined"
                            value={city}
                            onChange={handleCityChange}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <Typography><strong>{t('Country')}</strong></Typography>
                        <StyledTextField
                            variant="outlined"
                            value={state}
                            onChange={handleStateChange}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <>
                            <Stack direction="row" alignItems="center" justifyContent="space-between" spacing={2}>
                                <Typography>
                                    <strong>{t('FestivalRestaurants')}</strong>
                                </Typography>

                                <Button
                                    color="primary"
                                    variant="contained"
                                    size="small"
                                    onClick={handleOpenAddRestaurant}
                                >
                                    +
                                </Button>
                            </Stack>

                            {festivalRestaurants.map((fr: any, index: number) => (
                                <FestivalRestaurantItem
                                    key={index}
                                    festivalRestaurant={fr}
                                    onDelete={handleFestivalRestaurantDelete}
                                    onClick={handleFestivalRestaurantClick}
                                />
                            ))}
                        </>
                    </Grid>
                </Grid>

                {props?.action === 'UPDATE' && <>
                    <Divider />

                    <Button
                        color="black"
                        variant="text"
                        onClick={handleDeleteFestival}
                    >
                        {t('DeleteFestivalAction')}
                    </Button>
                </>}
            </Stack>
        </>
    );
}