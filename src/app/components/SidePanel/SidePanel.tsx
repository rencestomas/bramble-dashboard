import { Box, Button, Stack, styled, useTheme } from "@mui/material";
import * as React from "react";
import { useLocation } from "react-router-dom";
import CloseIcon from '@mui/icons-material/Close';
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../core/store/store";
import Typography from "@mui/material/Typography";
import { useState } from "react";
import MenuItemEditor from "../MenuItemEditor/MenuItemEditor";
import CategoryEditor from "../CategoryEditor/CategoryEditor";
import useAxiosSecure from "../../../hooks/useAxiosSecure";
import ConfirmDialog from "../ConfirmDialog/ConfirmDialog";
import ExtraMenuItemEditor from "../ExtraMenuItemEditor/ExtraMenuItemEditor";
import { newGuid } from "../../core/utilities";
import { MenuItemExtraCategory } from "../../shared/interfaces/menuitem-extra-category.interface";
import { useTranslation } from "react-multi-lang";
import { setPanelOpen } from "../../core/store/features/sidePanel/sidePanelSlice";
import { showLoading } from "../../core/store/features/loading/loadingSlice";
import { addCategory, addMenuitem, deleteCategory, updateCategory, updateMenuitem, upsertMenuitem } from "../../core/store/features/category/categorySlice";
import { addExtraCategory, addExtraMenuitem, deleteExtraCategory, deleteExtraMenuitem, updateExtraCategory, updateExtraMenuitem } from "../../core/store/features/extraCategory/extraCategorySlice";
import OrderDetails from "../OrderDetails/OrderDetails";
import { displayNotification } from "../../core/store/features/notification/notificationSlice";
import RestaurantEditor from "../RestaurantEditor/RestaurantEditor";
import FestivalEditor from "../FestivalEditor/FestivalEditor";
import { addFestival, deleteFestival, updateFestival } from "../../core/store/features/festival/festivalSlice";
import { addRestaurant, deleteRestaurant, deleteRestaurants, updateRestaurant } from "../../core/store/features/restaurant/restaurantSlice";
import DeleteSafeDialog from "../DeleteSafeDialog/DeleteSafeDialog";
import { Festival } from "../../shared/interfaces/festival.interface";
import { Restauration } from "../../shared/interfaces/restauration.interface";
import { SecurityRole } from "../../core/security/SecurityRole";


const BoldTypography = styled(Typography)(({ theme }) => ({
    fontWeight: 'bold'
}));


export interface SidePanelProps { }

export default function SidePanel(props: SidePanelProps) {

    const theme = useTheme();
    const location = useLocation();
    const dispatch = useDispatch();
    const axiosSecure = useAxiosSecure();
    const sidePanel = useSelector((state: RootState) => state.sidePanel);
    const auth = useSelector((state: RootState) => state.auth);
    const t = useTranslation();


    const [dataChange, setDataChange] = useState<any>(null);
    const [openConfirm, setOpenConfirm] = useState(false);
    const [openDeleteConfirm, setOpenDeleteConfirm] = useState(false);
    const [openSafeConfirm, setOpenSafeConfirm] = useState(false);
    const [deleteId, setDeleteId] = useState<string | null>(null);
    const [toDeleteItem, setToDeleteItem] = useState<Festival | Restauration | null>(null);


    function handleCloseSidePanel() {
        dispatch(setPanelOpen(false));
    }

    function handleSaveData() {
        setOpenConfirm(true);
    }

    function handleDelete(id: string) {
        setDeleteId(id);
        setOpenDeleteConfirm(true);
    };

    function handleSafeDelete(event: any) {
        setToDeleteItem(event);
        setDeleteId(event.id);
        setOpenSafeConfirm(true);
    };

    async function saveData() {

        switch (sidePanel.context?.contextType) {
            case "Category": {

                if (sidePanel?.context?.action === 'UPDATE') {
                    let response = await axiosSecure.put(`/api/category`, dataChange, true);
                    if (response?.data && response?.data?.length > 0) {
                        dispatch(updateCategory(response?.data[0]));
                    }
                }

                if (sidePanel?.context?.action === 'CREATE') {
                    let response = await axiosSecure.post(`/api/category`, {
                        ...dataChange,
                        restaurationId: auth.user?.restaurationId
                    }, true);

                    if (response?.data && response?.data?.length > 0) {
                        dispatch(addCategory(response?.data[0]));
                    }
                }

                break;
            }
            case "ExtraCategory": {
                if (sidePanel?.context?.action === 'UPDATE') {
                    let response = await axiosSecure.put(`/api/extra_category`, dataChange, true);
                    if (response?.data && response?.data?.length > 0) {
                        dispatch(updateExtraCategory(response?.data[0]));
                    }
                }

                if (sidePanel?.context?.action === 'CREATE') {
                    let response = await axiosSecure.post(`/api/extra_category`, {
                        ...dataChange,
                        restaurationId: auth.user?.restaurationId
                    }, true);

                    if (response?.data && response?.data?.length > 0) {
                        dispatch(addExtraCategory(response?.data[0]));
                    }
                }
                break;
            }
            case "Menuitem": {

                dispatch(showLoading({
                    showLoading: true
                }));

                if (sidePanel?.context?.action === 'UPDATE') {
                    let response = await axiosSecure.put(`/api/menuitem`, { ...dataChange, image: null }, false);
                    if (response?.data && response?.data?.length > 0) {
                        dispatch(updateMenuitem(response?.data[0]));
                    }
                }

                if (sidePanel?.context?.action === 'CREATE') {
                    let response = await axiosSecure.post(`/api/menuitem`, { ...dataChange, image: null }, false);
                    if (response?.data && response?.data?.length > 0) {
                        dispatch(addMenuitem(response?.data[0]));
                    }
                }

                let currentExtraCategories = sidePanel?.context?.contextObject?.menuitemExtraCategories;

                if (currentExtraCategories) {
                    let addedExtraCategoryIds = dataChange?._extraCategoryIds?.filter((ecId: string) =>
                        currentExtraCategories?.map((e: MenuItemExtraCategory) => e.extraCategoryId)?.indexOf(ecId) === -1)

                    let removedExtraCategories = currentExtraCategories?.filter((ec: MenuItemExtraCategory) =>
                        dataChange?._extraCategoryIds?.indexOf(ec.extraCategoryId) === -1);

                    let removedExtraCategoriesIds = removedExtraCategories?.map((r: MenuItemExtraCategory) => r.id)

                    if (addedExtraCategoryIds && addedExtraCategoryIds?.length > 0) {
                        let assign_response = await axiosSecure.put(`/api/menuitem_extra_category/many`,
                            addedExtraCategoryIds.map((extraCategoryId: string, index: number) => ({
                                id: newGuid(),
                                menuitemId: sidePanel?.context?.contextObject?.id,
                                extraCategoryId: extraCategoryId,
                                viewOrder: index
                            })), false);
                    }

                    if (removedExtraCategoriesIds && removedExtraCategoriesIds?.length > 0) {
                        for (let id of removedExtraCategoriesIds) {
                            let unssign_response = await axiosSecure.delete(`/api/menuitem_extra_category/${id}`, false);
                        }
                    }
                } else {
                    // non existing menuitem
                    let addedExtraCategoryIds = dataChange?._extraCategoryIds;

                    if (addedExtraCategoryIds && addedExtraCategoryIds?.length > 0) {
                        let assign_response = await axiosSecure.put(`/api/menuitem_extra_category/many`,
                            addedExtraCategoryIds.map((extraCategoryId: string, index: number) => ({
                                id: newGuid(),
                                menuitemId: sidePanel?.context?.contextObject?.id,
                                extraCategoryId: extraCategoryId,
                                viewOrder: index
                            })), false);
                    }
                }

                // upload image
                if (dataChange?.image && auth?.user?.restaurationId) {
                    let upload_res = await axiosSecure.uploadImage(`/api/thumbnail/upload_image`,
                        auth?.user?.restaurationId, dataChange?.id, dataChange?.image, false
                    ).catch(e => {
                        dispatch(displayNotification({
                            message: "Chyba pri nahrávaní súboru.",
                            severity: "error"
                        }));
                    })
                }

                // reload data
                let menuitem_res = await axiosSecure.get(`/api/menuitem?id=${sidePanel?.context?.contextObject?.id}`, false);
                if (menuitem_res?.data && menuitem_res?.data?.length > 0) {
                    dispatch(upsertMenuitem(menuitem_res?.data[0]));
                }

                dispatch(showLoading({
                    showLoading: false
                }));

                break;
            }
            case "ExtraMenuitem": {

                if (sidePanel?.context?.action === 'UPDATE') {
                    // update item
                    let response = await axiosSecure.put(`/api/extra_menuitem`, dataChange, true);
                    if (response?.data && response?.data?.length > 0) {
                        dispatch(updateExtraMenuitem(response?.data[0]));
                    }
                }

                if (sidePanel?.context?.action === 'CREATE') {

                    // existing item was overwritten during assignning
                    if (!dataChange?._isExisting) {
                        try {
                            let response = await axiosSecure.post(`/api/extra_menuitem`, {
                                ...dataChange,
                                restaurationId: auth?.user?.restaurationId
                            }, true);
                        } catch (e) {

                        }
                    }

                    let assign_response = await axiosSecure.post(`/api/extra_category_extra_menuitem`, {
                        extraCategoryId: sidePanel?.context?.contextObject?.extraCategoryId,
                        extraMenuitemId: dataChange?.id,
                        viewOrder: 0,
                        isHidden: false
                    }, true);

                    if (assign_response?.data && assign_response?.data?.length > 0) {
                        dispatch(addExtraMenuitem(assign_response?.data[0]));
                    }
                }
                break;
            }
            case "Festival": {

                dispatch(showLoading({
                    showLoading: true
                }));

                if (sidePanel?.context?.action === 'UPDATE') {
                    let response = await axiosSecure.put(`/api/festival`, dataChange, true);
                    if (response?.data && response?.data?.length > 0) {
                        dispatch(updateFestival(response?.data[0]));
                    }
                }

                if (sidePanel?.context?.action === 'CREATE') {
                    let response = await axiosSecure.post(`/api/festival`, {
                        ...dataChange
                    }, true);

                    if (response?.data && response?.data?.length > 0) {
                        dispatch(addFestival(response?.data[0]));
                    }
                }

                // upload image
                if (dataChange?.image && auth?.user?.restaurationId) {
                    let upload_res = await axiosSecure.uploadImage(`/api/thumbnail/upload_image`,
                        'festivals', dataChange?.id, dataChange?.image, false
                    ).catch(e => {
                        dispatch(displayNotification({
                            message: "Chyba pri nahrávaní súboru.",
                            severity: "error"
                        }));
                    })
                }

                let currentFestivalRestaurants = sidePanel?.context?.contextObject?.festivalRestaurants;

                if (currentFestivalRestaurants) {
                    let addedFestivalRestaurants = dataChange?.festivalRestaurants;

                    // ?.filter((fr: any) =>
                    //     currentFestivalRestaurants?.map((e: any) => e.restaurantId)?.indexOf(fr.restaurantId) === -1)

                    let removedFestivalRestaurants = currentFestivalRestaurants?.filter((fr: any) =>
                        dataChange?.festivalRestaurants?.map((fr: any) => fr.restaurantId)?.indexOf(fr.restaurantId) === -1);

                    if (addedFestivalRestaurants && addedFestivalRestaurants?.length > 0) {
                        let assign_response = await axiosSecure.put(`/api/festival_restaurant/many`,
                            addedFestivalRestaurants, false);
                    }

                    if (removedFestivalRestaurants && removedFestivalRestaurants?.length > 0) {
                        for (let id of removedFestivalRestaurants.map((fr: any) => fr.id)) {
                            let unssign_response = await axiosSecure.delete(`/api/festival_restaurant/${id}`, false);
                        }
                    }
                } else {
                    // non existing festivalRestaurant
                    let addedFestivalRestaurants = dataChange?.festivalRestaurants;

                    if (addedFestivalRestaurants && addedFestivalRestaurants?.length > 0) {
                        let assign_response = await axiosSecure.put(`/api/festival_restaurant/many`,
                            addedFestivalRestaurants, false);
                    }
                }

                dispatch(showLoading({
                    showLoading: false
                }));

                break;
            }
            case "Restaurant": {

                dispatch(showLoading({
                    showLoading: true
                }));

                if (sidePanel?.context?.action === 'UPDATE') {
                    let response = await axiosSecure.put(`/api/restauration`, dataChange, false);
                    if (response?.data && response?.data?.length > 0) {
                        dispatch(updateRestaurant(response?.data[0]));
                    }
                }

                if (sidePanel?.context?.action === 'CREATE') {

                    let response = await axiosSecure.post(`/api/restauration`, {
                        ...dataChange
                    }, false);

                    if (response?.data && response?.data?.length > 0) {
                        dispatch(addRestaurant(response?.data[0]));

                        // manager profile
                        let manager_response = await axiosSecure.post(`/api/user/register`, {
                            ...dataChange,
                            password1: 'admin',
                            password2: 'admin',
                            restaurationId: response?.data[0]?.id, // pair account with new restauration
                            securityRoleId: SecurityRole.Manager
                        }, false);

                        // staff profile
                        let staff_response = await axiosSecure.post(`/api/user/register`, {
                            ...dataChange,
                            id: newGuid(),
                            firstName: `${dataChange.firstName}_staff`,
                            lastName: `${dataChange.lastName}_staff`,
                            login: `${dataChange.login}_staff`,
                            password1: 'admin',
                            password2: 'admin',
                            restaurationId: response?.data[0]?.id, // pair account with new restauration
                            securityRoleId: SecurityRole.Kitchen
                        }, false);
                    }
                }

                // upload image
                if (dataChange?.image && auth?.user?.restaurationId) {
                    let upload_res = await axiosSecure.uploadImage(`/api/thumbnail/upload_image`,
                        'restaurants', dataChange?.id, dataChange?.image, false
                    ).catch(e => {
                        dispatch(displayNotification({
                            message: "Chyba pri nahrávaní súboru.",
                            severity: "error"
                        }));
                    })
                }

                dispatch(showLoading({
                    showLoading: false
                }));

                break;
            }
        }
    }

    async function deleteData() {
        if (deleteId) {
            switch (sidePanel.context?.contextType) {
                case "Category": {
                    await axiosSecure.delete(`/api/category/${deleteId}`, true);
                    dispatch(deleteCategory(deleteId))
                    break;
                }
                case "ExtraCategory": {
                    await axiosSecure.delete(`/api/extra_category/${deleteId}`, true);
                    dispatch(deleteExtraCategory(deleteId))
                    break;
                }
                case "Menuitem": {
                    break;
                }
                case "ExtraMenuitem": {
                    await axiosSecure.delete(`/api/extra_menuitem/${deleteId}`, true);
                    dispatch(deleteExtraMenuitem(deleteId))
                    break;
                }
                case "Festival": {
                    await axiosSecure.delete(`/api/festival/${deleteId}`, true);
                    dispatch(deleteFestival(deleteId))
                    break;
                }
                case "Restaurant": {
                    await axiosSecure.delete(`/api/restauration/${deleteId}`, true);
                    dispatch(deleteRestaurant(deleteId))
                    break;
                }
            }
        }
    }

    async function handleConfirmClose(value: boolean) {
        setOpenConfirm(false);
        if (value) {
            await saveData();
            dispatch(setPanelOpen(false));
        }
    };

    async function handleDeleteConfirmClose(value: boolean) {
        setOpenDeleteConfirm(false);
        if (value) {
            await deleteData();
            dispatch(setPanelOpen(false));
        }
    };


    async function handleSafeConfirmClose(event: any) {
        if (event) {
            if (toDeleteItem) {
                await deleteData();
                dispatch(setPanelOpen(false));
            }
        }

        setToDeleteItem(null);
        setDeleteId(null);
        setOpenSafeConfirm(false);
    }

    function handleChangeData(event: any) {
        setDataChange(event)
    }

    function getDeleteLabel() {
        let objectName: string;
        switch (sidePanel.context?.contextType) {
            case "Category":
                objectName = t('Category');
                break;
            case "ExtraCategory":
                objectName = t('ExtraCategory');
                break;
            case "Menuitem":
                objectName = t('Menuitem');
                break;
            case "ExtraMenuitem":
                objectName = t('ExtraMenuitem');
                break;
            case "Festival":
                objectName = t('Festival');
                break;
            case "Restaurant":
                objectName = t('Restaurant');
                break;
            default:
                objectName = "object";
                break;
        }

        return `${t('DeleteLabelQuestion')} ${objectName}?`;
    }

    return (
        <>
            <ConfirmDialog
                open={openConfirm}
                onClose={handleConfirmClose}
                title={t('SaveQuestionTitle')}
                content={t('SaveQuestion')}
                noLabel={t('CancelAction')}
                yesLabel={t('SaveAction')}
            />

            <ConfirmDialog
                open={openDeleteConfirm}
                onClose={handleDeleteConfirmClose}
                title={t('DeleteQuestionTitle')}
                content={getDeleteLabel()}
                noLabel={t('CancelAction')}
                yesLabel={t('DeleteAction')}
            />

            <DeleteSafeDialog
                open={openSafeConfirm}
                onClose={(e) => handleSafeConfirmClose(e)}
                title={t('DeleteQuestionTitle')}
                content={getDeleteLabel()}
                confirmPhrase={toDeleteItem?.name || ''}
                noLabel={t('KeepAction')}
                yesLabel={t('DeleteAction')}
            />

            <Box>
                <Stack
                    direction="row"
                    alignItems="center"
                    justifyContent="space-between"
                    spacing={2}
                    sx={{
                        height: '60px',
                        margin: '0px 10px',
                        background: 'white',
                        position: 'sticky',
                        top: 0,
                        zIndex: 2
                    }}
                >
                    <Stack
                        direction="row"
                        alignItems="center"
                        justifyContent="space-between"
                        spacing={2}
                    >
                        <CloseIcon
                            fontSize="small"
                            sx={{
                                cursor: 'pointer',
                                ml: 2
                            }}
                            onClick={handleCloseSidePanel}
                        />

                        {sidePanel.context?.contextType === "Category" &&
                            <>
                                <BoldTypography>
                                    {sidePanel?.context?.action === 'UPDATE' ? t('UpdateCategoryTitle') : t('CreateCategoryTitle')}
                                </BoldTypography>
                            </>
                        }

                        {sidePanel.context?.contextType === "Menuitem" &&
                            <>
                                <BoldTypography>
                                    {sidePanel?.context?.action === 'UPDATE' ? t('UpdateMenuitemTitle') : t('CreateMenuitemTitle')}
                                </BoldTypography>
                            </>
                        }

                        {sidePanel.context?.contextType === "ExtraCategory" &&
                            <>
                                <BoldTypography>
                                    {sidePanel.context.action === 'UPDATE' ? t('UpdateExtraCategoryTitle') : t('CreateExtraCategoryTitle')}
                                </BoldTypography>
                            </>
                        }

                        {sidePanel.context?.contextType === "ExtraMenuitem" &&
                            <>
                                <BoldTypography>
                                    {sidePanel.context.action === 'UPDATE' ? t('UpdateExtraMenuitemTitle') : t('CreateExtraMenuitemTitle')}
                                </BoldTypography>
                            </>
                        }

                        {sidePanel.context?.contextType === "Festival" &&
                            <>
                                <BoldTypography>
                                    {sidePanel.context.action === 'UPDATE' ? t('UpdateFestivalTitle') : t('CreateFestivalTitle')}
                                </BoldTypography>
                            </>
                        }

                        {sidePanel.context?.contextType === "Restaurant" &&
                            <>
                                <BoldTypography>
                                    {sidePanel.context.action === 'UPDATE' ? t('UpdateRestaurantTitle') : t('CreateRestaurantTitle')}
                                </BoldTypography>
                            </>
                        }
                    </Stack>

                    {sidePanel.context?.action !== 'VIEW' && <Button
                        color="primary"
                        variant="contained"
                        size="small"
                        onClick={handleSaveData}
                    >
                        {t('SaveChangesAction')}
                    </Button>}
                </Stack>

                <Box
                    sx={{
                        p: 2
                    }}
                >
                    {sidePanel.context?.contextType === "Category" &&
                        <>
                            <CategoryEditor
                                isExtraCategory={false}
                                data={sidePanel.context?.contextObject}
                                action={sidePanel.context?.action}
                                onChangeData={handleChangeData}
                                onDelete={handleDelete}
                            />
                        </>
                    }

                    {sidePanel.context?.contextType === "Menuitem" &&
                        <>
                            <MenuItemEditor
                                data={sidePanel.context.contextObject}
                                onChangeData={handleChangeData}
                            />
                        </>
                    }

                    {sidePanel.context?.contextType === "ExtraCategory" &&
                        <>
                            <CategoryEditor
                                isExtraCategory={true}
                                data={sidePanel.context?.contextObject}
                                action={sidePanel.context?.action}
                                onChangeData={handleChangeData}
                                onDelete={handleDelete}
                            />
                        </>
                    }

                    {sidePanel.context?.contextType === "ExtraMenuitem" &&
                        <>
                            <ExtraMenuItemEditor
                                action={sidePanel.context.action || "CREATE"}
                                data={sidePanel.context.contextObject}
                                extraCategory={sidePanel.context.extraData}
                                onChangeData={handleChangeData}
                                onDelete={handleDelete}
                            />
                        </>
                    }

                    {sidePanel.context?.contextType === "Restaurant" &&
                        <>
                            <RestaurantEditor
                                action={sidePanel.context.action || "CREATE"}
                                data={sidePanel.context.contextObject}
                                onChangeData={handleChangeData}
                                onDelete={handleSafeDelete}
                            />
                        </>
                    }

                    {sidePanel.context?.contextType === "Festival" &&
                        <>
                            <FestivalEditor
                                action={sidePanel.context.action || "CREATE"}
                                data={sidePanel.context.contextObject}
                                onChangeData={handleChangeData}
                                onDelete={handleSafeDelete}
                            />
                        </>
                    }

                    {sidePanel.context?.contextType === "Order" &&
                        <>
                            <OrderDetails
                                data={sidePanel.context.contextObject}
                                showActions={true}
                            />
                        </>
                    }
                </Box>

            </Box >
        </>
    );
}