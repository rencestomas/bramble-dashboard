import * as React from 'react';
import { Button, capitalize, FormControl, IconButton, InputAdornment, InputLabel, ListItem, ListItemButton, ListItemIcon, ListItemText, MenuItem, Select, SelectChangeEvent, Stack, Typography } from '@mui/material';
import { useEffect, useState } from 'react';
import dayjs, { Dayjs } from 'dayjs';
import { getDayName } from '../../core/utilities';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

import NavigateBeforeIcon from '@mui/icons-material/NavigateBefore';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import StackGrid from 'react-stack-grid';
import { useTranslation } from 'react-multi-lang';

export interface DatePickerProps {
    onChange: (event: any[]) => void;
}

export default function DatePicker(props: DatePickerProps) {

    const [weekStart, setWeekStart] = useState<Dayjs>(dayjs().startOf('week').add(1, 'day'));
    const [week, setWeek] = useState<string[]>([]);
    const t = useTranslation();
    

    useEffect(() => {
        setWeek(getWeek());
    }, [weekStart])


    useEffect(() => {
        if (week && week.length)
            props?.onChange(week);
    }, [week])


    function getWeek() {
        const start = weekStart?.toDate();
        const end = weekStart?.endOf('week').toDate();

        const listDate = [];

        let currentDate = dayjs(start);
        listDate.push(currentDate.format().slice(0, 10));


        while (currentDate.isBefore(end) || currentDate.isSame(end)) {
            currentDate = currentDate.add(1, 'day');
            listDate.push(currentDate.format().slice(0, 10));
        }

        return listDate;
    }


    function handleNext(event: any) {
        setWeekStart(weekStart?.add(1, 'week'));
    }

    function handlePrevious(event: any) {
        setWeekStart(weekStart?.add(-1, 'week'));
    }

    return (
        <>
            <Stack
                spacing={2}
            >
                <Stack
                    direction="row"
                    alignItems="center"
                    justifyContent="space-between"
                    spacing={2}
                >
                    <IconButton
                        onClick={handlePrevious}
                    >
                        <NavigateBeforeIcon />
                    </IconButton>

                    <Typography><strong>{t('Week')} {dayjs(week[0]).format("DD.MM.YYYY")} - {dayjs(week[week.length - 1]).format("DD.MM.YYYY")}</strong></Typography>

                    <IconButton
                        onClick={handleNext}
                    >
                        <NavigateNextIcon />
                    </IconButton>
                </Stack>

{/* 
                <StackGrid
                    columnWidth={80}
                >
                    {week?.map((x, i) => (
                        <Button
                            key={i}
                            color="primary"
                        >
                            {capitalize(getDayName(x, 'sk-SK'))} {dayjs(x).format("DD.MM")}
                        </Button>
                    ))}
                </StackGrid> */}
            </Stack>
        </>
    );
}