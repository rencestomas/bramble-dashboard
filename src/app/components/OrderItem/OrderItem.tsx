import { Button, Card, CardContent, IconButton, Typography, useTheme } from "@mui/material";
import { useState } from "react";
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import { Order } from "../../shared/interfaces/order.interface";
import { useTranslation } from "react-multi-lang";
import { setPanelContext } from "../../core/store/features/sidePanel/sidePanelSlice";
import { useDispatch } from "react-redux";
import { OrderStateEnum } from "../../shared/interfaces/order-state.interface";
import AlertDialog from "../AlertDialog/AlertDialog";


export interface OrderItemProps {
    order: Order;
    // onApproveAndPrepare: (order: Order | null) => void;
    // onDone: (order: Order | null) => void;
    // onCancel: (order: Order | null, orderStateReason: string) => void;
}

export default function OrderItem(props: OrderItemProps) {

    const theme = useTheme();
    const [openAlert, setOpenAlert] = useState(false);
    const t = useTranslation()
    const dispatch = useDispatch();

    const { order } = props;


    const handleOrderClick = () => {
        dispatch(setPanelContext({
            action: 'VIEW',
            contextType: 'Order',
            contextObject: props?.order
        }));
    };

    const handleAlertClose = () => {
        setOpenAlert(false);
    }

    const handleDetailsClick = (event: any) => {
        event.stopPropagation();
        setOpenAlert(true);
    }

    function getOrderStateLabel() {

        switch (props?.order?.orderStateId) {
            case OrderStateEnum.IsCreated:
                return t('IsWaitingForApproval');
            case OrderStateEnum.IsPreparing:
                return t('IsPreparing');
            case OrderStateEnum.IsCanceled:
                return t('IsCanceled');
            case OrderStateEnum.IsDone:
                return t('IsDone');
            default:
                return t('IsWaitingForApproval');
        }
    }

    function getOrderColor() {

        switch (props?.order?.orderStateId) {
            case OrderStateEnum.IsCreated:
                return theme.palette.primary.main;
            case OrderStateEnum.IsPreparing:
                return theme.palette.warning.main;
            case OrderStateEnum.IsCanceled:
                return theme.palette.natural.main;
            case OrderStateEnum.IsDone:
                return theme.palette.secondary.main;
            default:
                return theme.palette.primary.main;
        }
    }

    function getButtonColor() {
        switch (props?.order?.orderStateId) {
            case OrderStateEnum.IsCreated:
                return "primary";
            case OrderStateEnum.IsPreparing:
                return "warning";
            case OrderStateEnum.IsCanceled:
                return "natural";
            case OrderStateEnum.IsDone:
                return "secondary";
            default:
                return "primary";
        }
    }

    const color = getOrderColor();

    return (
        <>
            {openAlert && <AlertDialog
                open={openAlert}
                onClose={handleAlertClose}
                title={t('Info')}
                content={props?.order?.orderStateReason}
                okLabel={t('Ok')}
            />}

            <Card sx={{
                aspectRatio: "1/ 1",
                maxHeight: "300px",
                borderRadius: "10px",
                textAlign: "center",
                padding: "20px",
                margin: "5px",
                display: 'flex',
                justiyContent: 'space-between',
                flexDirection: 'column',
                boxShadow: '0px 2px 6px rgba(13, 10, 44, 0.08)',
                ':hover': {
                    boxShadow: 15,
                    cursor: 'pointer'
                }
            }}
                onClick={handleOrderClick}
            >
                <CardContent
                    sx={{
                        height: "100%"
                    }}
                >
                    {/* <Stack
                        spacing={1}
                        direction="row"
                        justifyContent="center"
                        alignItems="center"
                    > */}
                    <Typography
                        sx={{
                            fontSize: {
                                lg: 18,
                                md: 15
                            },
                            fontWeight: 'bold'
                        }}
                        color={color}
                    >
                        {getOrderStateLabel()}
                        {props?.order?.orderStateId === OrderStateEnum.IsCanceled && <IconButton onClick={(e) => handleDetailsClick(e)} >
                            <InfoOutlinedIcon fontSize="small" />
                        </IconButton>}
                    </Typography>


                    {/* </Stack> */}

                    <Typography
                        sx={{
                            fontSize: {
                                lg: 48,
                                md: 48,
                                sm: 48,
                                xs: 48
                            },
                            fontWeight: 'bold'
                        }}
                        color={color}
                    >
                        {props?.order?.orderNumber}
                        {/* {props?.order?.table?.number} */}
                    </Typography>
                    <Typography
                        sx={{
                            fontSize: {
                                lg: 18,
                                md: 15
                            },
                            fontWeight: 'bold'
                        }}
                        color={color}
                    >
                        {t('TableNumber')}
                    </Typography>

                </CardContent>

                <>
                    {props?.order?.orderStateId === OrderStateEnum.IsPreparing ? (
                        <Button
                            fullWidth
                            color={getButtonColor()}
                            variant="contained"
                            onClick={handleOrderClick}
                        >
                            {t('IsDone')}
                        </Button>
                    ) : (
                        <Button
                            fullWidth
                            color={getButtonColor()}
                            variant="contained"
                            onClick={handleOrderClick}
                        >
                            {t('Details')}
                        </Button>
                    )}

                </>
            </Card>

            {/* <OrderDialog
                open={openDetail}
                showActions={true}
                onClose={handleDetailClose}
                onApproveAndPrepare={handleApproveAndPrepare}
                onDone={handleDone}
                onCancel={handleCancel}
                data={props.order}
            /> */}
        </>
    );
}