import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from "react";
import { Chip, Grid, IconButton, ImageList, ImageListItem, ImageListItemBar, ListSubheader, MenuItem, Select, Stack, styled, TextField, useTheme } from "@mui/material";
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import DeleteIcon from '@mui/icons-material/Delete';
import FileUploading from 'react-files-uploading';
import { Menuitem } from '../../shared/interfaces/menuitem.interface';
import useAxiosSecure from '../../../hooks/useAxiosSecure';
import { useSelector } from 'react-redux';
import { RootState } from '../../core/store/store';
import { ExtraCategory } from '../../shared/interfaces/extra-category.interface';
import { useTranslation } from 'react-multi-lang';
import { MINIO_URL } from '../../core/api/axios';


const StyledTextField = styled(TextField)(({ theme }) => ({
    background: 'white',
    width: '100%'
}));


const ExtraCategoryToggleButton = styled(ToggleButton)(({ theme }) => ({
    textTransform: "none",
    background: theme.palette.backgroundColor.main,
    border: '1px solid #EBEBF0',
    padding: '4px 8px',
    height: '42px',
    minWidth: '60px',
    borderRadius: '1000px',
    marginRight: '5px',
    marginTop: '5px'
}));


const DietToggleButtonGroup = styled(ToggleButtonGroup)(({ theme }) => ({
    display: "grid",
    gridTemplateColumns: "auto auto auto",
    gridGap: "10px",
    padding: "10px",
    ".MuiToggleButtonGroup-grouped": {
        border: '1px solid #EBEBF0 !important',
        borderRadius: '4px !important'
    }
}));

const AllergensBox = styled(Box)(({ theme }) => ({
    display: "grid",
    gridTemplateColumns: "repeat(auto-fill, minmax(30px, 1fr))",
    justifyContent: "start"
}));


export interface MenuItemEditorProps {
    data: Menuitem;
    onChangeData: (data: any) => void;
}

export default function MenuItemEditor(props: MenuItemEditorProps) {

    const theme = useTheme();

    const [name, setName] = useState(props?.data?.name || "");
    const [description, setDescription] = useState(props?.data?.description || "");
    const [weight, setWeight] = useState(parseWeight(props?.data?.weight) || 0);
    const [price, setPrice] = useState(props?.data?.price || 0);
    const [weightUnit, setWeightUnit] = useState(parseWeightUnit(props?.data?.weight) || "g");
    const [fileToUpload, setFileToUpload] = useState<File | null>(null);
    const [diet, setDiet] = useState<string | null>(null);
    const [thumbnail, setThumbnail] = useState<string>("../../../../src/assets/image_placeholder.png");


    const [extraCategories, setExtraCategories] = useState<ExtraCategory[]>([]);
    const [extraCategoriesMap, setExtraCategoriesMap] = useState<any>({});

    const axiosSecure = useAxiosSecure();
    const auth = useSelector((state: RootState) => state.auth);
    const t = useTranslation();


    let _parsedAllergens = parseAllergens(props?.data?.allergens);
    let _allergens: any = {};

    [...Array(14)].map((x, i) => {
        if (_parsedAllergens?.indexOf(`${i + 1}`) > -1) {
            _allergens[(i + 1)] = true;
        } else {
            _allergens[(i + 1)] = false;
        }
    });

    const [allergens, setAllergens] = useState<{
        [k: number]: boolean;
    }>(_allergens);


    useEffect(() => {
        async function apiCall() {

            let filter = "";

            if (auth.user) {
                filter += `restaurationId=${auth.user?.restaurationId}&`;
            }

            let response = await axiosSecure.get(`/api/extra_category?${filter}`, true);

            setExtraCategories(response.data)

            let _map: any = {};
            response.data.reduce(function (result: any, currentObject: any) {
                _map[currentObject.id] = false;
                return result;
            }, {});

            let selectedExtraCategories = props?.data?.menuitemExtraCategories?.map(i => i.extraCategoryId);

            for (const [key, value] of Object.entries(_map)) {
                if (selectedExtraCategories?.indexOf(key) > -1) {
                    _map[key] = true;
                } else {
                    _map[key] = false;
                }
            }

            setExtraCategoriesMap(_map);
        }

        apiCall();
    }, [])


    useEffect(() => {

        setName(props?.data?.name || "");
        setDescription(props?.data?.description || "");
        setWeight(parseWeight(props?.data?.weight) || 0);
        setPrice(props?.data?.price || 0);
        setDiet(props?.data?.diet?.toString() || null);
        setWeightUnit(parseWeightUnit(props?.data?.weight) || "g");
        setFileToUpload(null);
        setThumbnail(`${MINIO_URL}/${auth.user?.restaurationId}/${props?.data?.id}.jpg`);

        let selectedExtraCategories = props?.data?.menuitemExtraCategories?.map(i => i.extraCategoryId);

        for (const [key, value] of Object.entries(extraCategoriesMap)) {
            if (selectedExtraCategories?.indexOf(key) > -1) {
                extraCategoriesMap[key] = true;
            } else {
                extraCategoriesMap[key] = false;
            }
        }

        setExtraCategoriesMap(extraCategoriesMap);

        let _parsedAllergens = parseAllergens(props?.data?.allergens);
        let _allergens: any = {};

        [...Array(14)].map((x, i) => {
            if (_parsedAllergens?.indexOf(`${i + 1}`) > -1) {
                _allergens[(i + 1)] = true;
            } else {
                _allergens[(i + 1)] = false;
            }
        });
        setAllergens(_allergens)

    }, [props?.data])


    useEffect(() => {

        var checkedAllergens = Object.entries(allergens).filter(([k, v]) => v)?.map(([k, v]) => k)?.join(',');
        var checkedExtraCategories = Object.entries(extraCategoriesMap).filter(([k, v]) => v)?.map(([k, v]) => k);

        props?.onChangeData({
            id: props?.data?.id,
            name,
            description,
            weight: `${weight}${weightUnit}`,
            allergens: checkedAllergens,
            price,
            categoryId: props?.data?.categoryId,
            viewOrder: props?.data?.viewOrder || 0,
            isHidden: false,
            diet: diet == null ? -1 : +diet,
            image: fileToUpload ? fileToUpload : null,
            _extraCategoryIds: checkedExtraCategories
        });

    }, [name, description, weight, weightUnit, allergens, price, diet, extraCategoriesMap, fileToUpload])


    const handleAllergenChange = (event: any) => {
        let _allergens = { ...allergens };
        _allergens[event?.target?.value] = !_allergens[event?.target?.value];

        setAllergens(_allergens);
    }

    const handleDietChange = (
        event: React.MouseEvent<HTMLElement>,
        newDiet: string | null,
    ) => {
        setDiet(newDiet);
    };

    function parseWeight(weight: string | undefined) {
        var res = weight?.replace(/[^\d.-]/g, "");
        return res || "";
    }

    function parseWeightUnit(weight: string | undefined) {
        var res = weight?.replace(/[\W\d]/g, "");
        return res || "";
    }

    function parseAllergens(allergens: string | undefined) {
        var res = allergens?.split(",") || [];
        return res;
    }

    function handleNameChange(event: any) {
        setName(event.target.value);
    }

    function handleDescriptionChange(event: any) {
        setDescription(event.target.value);
    }

    function handlePriceChange(event: any) {
        setPrice(event.target.value);
    }

    function handleWeightChange(event: any) {
        setWeight(event.target.value);
    }

    function handleWeightUnitChange(event: any) {
        setWeightUnit(event.target.value);
    }

    function handleFileChange(fileList: File[]) {
        setFileToUpload(fileList[0]);
        const objectUrl = URL.createObjectURL(fileList[0]);
        setThumbnail(objectUrl);
        // setPreview(objectUrl)

        // free memory when ever this component is unmounted
        // return () => URL.revokeObjectURL(objectUrl)
    };

    function handleRemoveMenuitemThumbnailClick(event: any) {
        event.stopPropagation();
        setFileToUpload(null);
        setThumbnail('');
    }

    function handleExtraCategoryChange(event: any) {
        let _extraCategoriesMap = { ...extraCategoriesMap };
        _extraCategoriesMap[event?.target?.value] = !_extraCategoriesMap[event?.target?.value];
        setExtraCategoriesMap(_extraCategoriesMap);
    }

    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <Typography><strong>{t('Title')}</strong></Typography>
                <StyledTextField
                    variant="outlined"
                    value={name}
                    onChange={handleNameChange}
                />
            </Grid>

            <Grid item xs={8}>
                <Typography><strong>{t('Description')}</strong></Typography>
                <StyledTextField
                    variant="outlined"
                    multiline
                    rows={4}
                    value={description}
                    onChange={handleDescriptionChange}
                />
            </Grid>

            <Grid item xs={4}>
                <Typography><strong>{t('Image')}</strong></Typography>
                <FileUploading value={fileToUpload != null ? [fileToUpload] : []} onChange={handleFileChange}
                    acceptType={['jpg', 'jpeg', 'png']} maxFileSize={2097152}>
                    {({
                        fileList,
                        errors,
                        isDragging,
                        onFileUpload,
                        onFileRemoveAll,
                        onFileUpdate,
                        onFileRemove,
                        dragProps,
                    }) => {
                        return (
                            <>
                                <Box
                                    id="btn-upload"
                                    onClick={onFileUpload}
                                    {...dragProps}
                                    sx={{
                                        background: 'white',
                                        position: 'relative',
                                        color: '#AEAEB2', // theme.palette.primary.main,
                                        borderRadius: '8px',
                                        border: `3px dashed #AEAEB2`, // ${theme.palette.primary.main}`,
                                        height: '125px',
                                        textAlign: 'center',
                                        cursor: 'pointer',
                                        transition: 'filter ease-in-out 200ms',
                                        ':hover': {
                                            filter: 'brightness(0.85)'
                                        }
                                    }}
                                >
                                    {errors && errors.maxNumber && (
                                        <span>Number of selected files exceed maxNumber</span>
                                    )}

                                    <>
                                        <Stack
                                            spacing={2}
                                            direction="column"
                                            justifyContent="space-between"
                                            sx={{
                                                position: 'absolute',
                                                zIndex: 1,
                                                color: 'white',
                                                left: '50%',
                                                top: '50%',
                                                transform: 'translate(-50%, -50%)'
                                            }}
                                        >
                                            <Typography
                                                sx={{
                                                    textOverflow: 'ellipsis',
                                                    overflow: 'hidden',
                                                    whiteSpace: 'nowrap'
                                                }}
                                            >
                                                Click here or drop
                                            </Typography>

                                            {fileToUpload != null && (
                                                <Stack
                                                    spacing={2}
                                                    direction="row"
                                                    justifyContent="space-between"
                                                >
                                                    <Typography
                                                        sx={{
                                                            textOverflow: 'ellipsis',
                                                            overflow: 'hidden',
                                                            whiteSpace: 'nowrap',
                                                            maxWidth: '100px'
                                                        }}
                                                    >
                                                        {fileToUpload?.name}

                                                    </Typography>

                                                    <DeleteIcon
                                                        onClick={(e) => handleRemoveMenuitemThumbnailClick(e)} />
                                                </Stack>)}
                                        </Stack>
                                        <img
                                            style={{
                                                filter: "brightness(70%)",
                                                borderRadius: "5px",
                                                height: "100%",
                                                width: "100%",
                                                objectFit: "cover"
                                            }}
                                            src={thumbnail}
                                            onError={({ currentTarget }) => {
                                                currentTarget.onerror = null; // prevents looping
                                                currentTarget.src = require("../../../../src/assets/image_placeholder.png");
                                            }}
                                        />

                                    </>

                                </Box>
                            </>
                        );
                    }}
                </FileUploading>
            </Grid>

            <Grid item xs={6}>
                <Typography><strong>{t('Price')}</strong></Typography>
                <StyledTextField
                    type="number"
                    variant="outlined"
                    value={price}
                    onChange={handlePriceChange}
                />
            </Grid>

            <Grid item xs={6}>
                <Typography><strong>{t('SalePrice')}</strong></Typography>
                <StyledTextField
                    type="number"
                    variant="outlined"
                    value={price}
                    onChange={handlePriceChange}
                />
            </Grid>
            {/* </Stack> */}

            <Grid item xs={12}>
                <Typography><strong>{t('Size')}</strong></Typography>
                <Stack
                    direction="row"
                    spacing={2}
                    sx={{
                        background: 'white',
                        width: '100%'
                    }}
                >
                    <StyledTextField
                        type="number"
                        value={weight}
                        variant="outlined"
                        onChange={handleWeightChange}
                    />
                    <Select
                        value={weightUnit}
                        onChange={handleWeightUnitChange}
                    >
                        <MenuItem value="g">g</MenuItem>
                        <MenuItem value="ml">ml</MenuItem>
                        <MenuItem value="l">l</MenuItem>
                    </Select>
                </Stack>
            </Grid>

            <Grid item xs={12}>
                <Typography>
                    <strong>{t('Diet')}</strong>
                </Typography>

                <DietToggleButtonGroup
                    color="primary"
                    value={diet}
                    exclusive
                    onChange={handleDietChange}
                    aria-label="Platform"
                >
                    <ToggleButton value="0">Vegan</ToggleButton>
                    <ToggleButton value="1">Vegetarian</ToggleButton>
                    <ToggleButton value="2">Gluten-free</ToggleButton>
                </DietToggleButtonGroup>
            </Grid>

            <Grid item xs={12}>

                <Typography>
                    <strong>{t('Allergens')}</strong>
                </Typography>

                <AllergensBox>
                    {[...Array(14)].map((x, i) => (
                        <Grid item xs key={i + 1}>

                            <ToggleButton
                                sx={{
                                    width: "20px",
                                    height: "20px"
                                }}
                                color="primary"
                                value={i + 1}
                                selected={allergens[(i + 1)]}
                                onChange={handleAllergenChange}
                            >
                                {(i + 1)}
                            </ToggleButton>

                        </Grid>
                    ))}
                </AllergensBox>

            </Grid>

            <Grid item xs={12}>
                <>
                    <Typography>
                        <strong>{t('AvailableModifications')}</strong>
                    </Typography>

                    <Box
                        sx={{
                            display: 'flex',
                            flexWrap: 'wrap'
                        }}
                    >
                        {extraCategories.map((extraCategory, index) => (
                            <ExtraCategoryToggleButton
                                key={index}
                                color="primary"
                                value={extraCategory?.id}
                                selected={extraCategoriesMap[extraCategory?.id]}
                                onChange={handleExtraCategoryChange}
                            >
                                {extraCategory.name}
                            </ExtraCategoryToggleButton>
                        ))}
                    </Box>
                </>
            </Grid>
        </Grid>
    );
}