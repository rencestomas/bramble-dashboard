import {
    Avatar,
    Box,
    Grid,
    Button,
    CardActions,
    CardContent,
    Chip, Dialog,
    DialogActions,
    DialogContent,
    DialogTitle, Divider,
    List,
    ListItem,
    ListItemAvatar,
    ListItemText,
    Paper,
    Stack,
    styled,
    Typography,
    useTheme,
    ButtonProps,
    Slide,
    AccordionDetails,
    CardMedia,
    Card,
    ListItemButton
} from "@mui/material";

import CloseIcon from '@mui/icons-material/Close'
import { TransitionProps } from "@mui/material/transitions";
import React, { useEffect, useState } from "react";
import { Restauration } from "../../shared/interfaces/restauration.interface";
import { Category } from "../../shared/interfaces/category.interface";
import { useSelector } from "react-redux";
import useAxiosSecure from "../../../hooks/useAxiosSecure";
import { RootState } from "../../core/store/store";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import StarIcon from '@mui/icons-material/Star';
import MuiAccordion, { AccordionProps } from '@mui/material/Accordion';
import MuiAccordionSummary, {
    AccordionSummaryProps,
} from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';


export interface RestaurationDialogProps {
    open: boolean;
    isStandingOffer: boolean;
    data: Restauration | null;
    onClose: () => void;
}

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const Accordion = styled((props: AccordionProps) => (
    <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
    border: `1px solid ${theme.palette.divider}`,
    '&:not(:last-child)': {
        borderBottom: 0,
    },
    '&:before': {
        display: 'none',
    },
}));

const AccordionSummary = styled((props: AccordionSummaryProps) => (
    <MuiAccordionSummary
        expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
        {...props}
    />
))(({ theme }) => ({
    backgroundColor:
        theme.palette.mode === 'dark'
            ? 'rgba(255, 255, 255, .05)'
            : 'rgba(0, 0, 0, .03)',
    flexDirection: 'row-reverse',
    '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
        transform: 'rotate(-90deg)',
    },
    '& .MuiAccordionSummary-content': {
        marginLeft: theme.spacing(1),
    },
}));

export default function RestaurationDialog(props: RestaurationDialogProps) {

    const theme = useTheme();
    const { onClose, open } = props;

    const [categories, setCategories] = useState<Category[]>([]);

    const axiosSecure = useAxiosSecure();

    useEffect(() => {
        async function apiCall() {

            let filter = "";

            if (props?.data?.id) {
                filter += `restaurationId=${props?.data?.id}&`;
                filter += `isStandingOffer=${props?.isStandingOffer}`;

                let response = await axiosSecure.get(`/api/category?${filter}`, true);
                setCategories(response.data);
            }
        }

        apiCall();
    }, [])

    const handleClose = () => {
        onClose();
    };

    return (
        <>
            {props?.data != null && <>
                <Dialog
                    fullWidth
                    maxWidth="lg"
                    TransitionComponent={Transition}
                    onClose={handleClose}
                    open={open}
                    PaperProps={{
                        sx: {
                            height: "80vh",
                            width: "80vw"
                        }
                    }}
                >
                    {/* <DialogTitle>Order Detail</DialogTitle> */}

                    <DialogContent sx={{
                        padding: "0px"
                    }}>
                        <div style={{
                            borderWidth: "50px",
                            borderStyle: "solid",
                            borderColor: `${theme.palette.backgroundColor.main} ${theme.palette.backgroundColor.main} transparent transparent`,
                            position: "absolute",
                            zIndex: 999,
                            top: "0",
                            right: "0",
                            width: "0",
                            height: "0",
                            filter: "drop-shadow(0 1px 15px gray)"
                        }}
                        >
                            <div style={{
                                position: "relative",
                                top: "-30px",
                                left: "8px",
                                cursor: "pointer"
                            }}
                                onClick={handleClose}
                            >
                                <CloseIcon />
                            </div>
                        </div>

                        <Card elevation={0}>
                            <CardMedia
                                sx={{ height: 250 }}
                                image={`${"https://source.unsplash.com/1280x720/?restaurant"}?${Math.random()}`}
                            />
                            <CardContent>
                                <Stack direction="row" alignItems="center" spacing={2}>

                                    <Typography gutterBottom variant="h5" component="div">
                                        {props.data.name}
                                    </Typography>

                                    <Stack direction="row" alignItems="center" spacing={1}>
                                        <Chip
                                            label={props.data.isOpen ? 'Otvorené' : 'Zatvorené'}
                                            size="small"
                                            variant="outlined"
                                            color={props.data.isOpen ? 'success' : 'error'}
                                        ></Chip>

                                        <Chip label={'4'} icon={<StarIcon />} size="small" variant="outlined" color="success"></Chip>

                                    </Stack>
                                </Stack>

                                <Typography gutterBottom variant="body2">
                                    {props.data.city} {props.data.street}
                                </Typography>

                                <Stack spacing={2}>
                                    <Stack direction="row" alignItems="center" spacing={1}>
                                        {props.data.cuisines.map((cuisine, index) => (
                                            <Chip
                                                key={index}
                                                size="small"
                                                label={cuisine.name}
                                                sx={{
                                                    background: theme.palette.natural.main,
                                                    color: 'white'
                                                }}
                                            ></Chip>
                                        ))}
                                    </Stack>

                                    <Divider />
                                </Stack>

                                {categories.map((category, index) => (
                                    <Accordion
                                        key={category.id}
                                        sx={{
                                            zIndex: 998
                                        }}
                                    >
                                        <AccordionSummary
                                            expandIcon={<ExpandMoreIcon />}
                                        >
                                            <Typography>{category.name}</Typography>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <List>
                                                {category.menuitems.map((menuitem, index) => (
                                                    <ListItemButton
                                                        key={menuitem.id}
                                                        sx={{
                                                            width: '100%'
                                                        }}
                                                    >
                                                        <Stack
                                                            spacing={2}
                                                            direction="row"
                                                            alignItems="center"
                                                            justifyContent="space-between"
                                                            sx={{
                                                                width: '100%'
                                                            }}
                                                        >
                                                            <Typography fontSize="small">
                                                                <strong>{menuitem.name}</strong>
                                                            </Typography>

                                                            <Stack direction="row" alignItems="center" spacing={2}>
                                                                <Typography fontSize="small">
                                                                    <span>{menuitem.weight}</span>
                                                                </Typography>
                                                                <Typography fontSize="small">
                                                                    <strong>{menuitem.price}€</strong>
                                                                </Typography>
                                                            </Stack>
                                                        </Stack>
                                                    </ListItemButton>
                                                ))}
                                            </List>
                                        </AccordionDetails>
                                    </Accordion>

                                ))}
                            </CardContent>
                        </Card>


                    </DialogContent>

                    {/* <DialogActions sx={{
                        background: theme.palette.backgroundColor.secondary,
                        justifyContent: "space-between",
                        padding: "24px"
                    }}>
                        <Stack direction="row" spacing={1}>
                            (
                                <>
                                    <Button
                                        color="natural"
                                        variant="outlined"
                                        size="small"
                                        onClick={handleClose}
                                    >
                                        Zavrieť okno
                                    </Button>
                                    <Button
                                        color="secondary"
                                        variant="contained"
                                        size="small"
                                        autoFocus
                                        onClick={handleDone}
                                    >
                                        Vybavené
                                    </Button>
                                </>
                            )
                        </Stack>

                    </DialogActions> */}

                </Dialog >

            </>
            }
        </>
    );
}