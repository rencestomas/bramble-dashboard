import { Box, Button, Grid, Link, Stack, styled, TextField, Typography } from "@mui/material";
import { useFormInputValidation } from "react-form-input-validation";
import { useTranslation } from "react-multi-lang";
import { useDispatch } from 'react-redux';
import { useNavigate } from "react-router-dom";
import { register } from "../../core/store/features/auth/authService";
import { displayNotification } from "../../core/store/features/notification/notificationSlice";
import { newGuid } from "../../core/utilities";
import LanguageSelect from "../LanguageSelect/LanguageSelect";
import useAxiosSecure from "../../../hooks/useAxiosSecure";
import { SecurityRole } from "../../core/security/SecurityRole";


const SmallBoldTypography = styled(Typography)(({ theme }) => ({
    fontSize: 'small',
    fontWeight: 'bold'
}));

export default function FestivalRegister() {

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const t = useTranslation();
    const axiosSecure = useAxiosSecure();


    const [fields, errors, form] = useFormInputValidation({
        name: "",
        siteUrl: "",
        email: "",
        password1: "",
        password2: "",
        login: "",
        firstName: "",
        lastName: "",
        phone: "",
        street: "",
        zipCode: "",
        city: "",
        state: "SK"
    }, {
        name: "required",
        siteUrl: "required",
        email: "required|email",
        password1: "required",
        password2: "required",
        login: "required",
        firstName: "required",
        lastName: "required",
        phone: "required",
        street: "required",
        zipCode: "required",
        city: "required",
        state: "required"
    });

    async function onSubmit(event: any) {
        const isValid = await form.validate(event);

        if (isValid) {

            let dataChange = {
                id: newGuid(),
                name: (fields as any).name,
                street: (fields as any).street,
                siteUrl: (fields as any).siteUrl,
                zipCode: (fields as any).zipCode,
                city: (fields as any).city,
                state: (fields as any).state
            };

            let response = await axiosSecure.post(`/api/festival`, dataChange, false);

            if (response?.data && response?.data?.length > 0) {

                // manager profile
                let manager_response = await axiosSecure.post(`/api/user/register`, {
                    ...dataChange,
                    id: newGuid(),
                    password1: (fields as any).password1,
                    password2: (fields as any).password2,
                    login: (fields as any).login,
                    firstName: (fields as any).firstName,
                    lastName: (fields as any).lastName,
                    email: (fields as any).email,
                    phone: (fields as any).phone,
                    festivalId: response?.data[0]?.id, // pair account with new festival
                    securityRoleId: SecurityRole.Manager
                }, false);
            }

            dispatch(displayNotification({
                message: t('FestivalCreated'),
                autoHideDuration: 5000,
                severity: "success"
            }));

            navigate("/login");
        }

    }

    return (
        <Box
            sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
            }}
        >
            <Stack>
                <LanguageSelect
                    text={t('Language')}
                    open={true}
                    onMouseOver={() => { }}
                    onMouseOut={() => { }}
                />

                <Grid
                    container
                    rowSpacing={4}
                    columnSpacing={4}
                    component="form"
                    onSubmit={onSubmit}
                    autoComplete="off"
                >
                    <Grid container item xs={6} direction="column" >
                        <Stack spacing={2}>
                            <TextField
                                required
                                label='Name'
                                name="name"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).name}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).name
                                    ? (errors as any).name
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label="Email"
                                name="email"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).email}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).email
                                    ? (errors as any).email
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("Password")}
                                name="password1"
                                type="password"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).password1}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).password
                                    ? (errors as any).password
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("RepeatPassword")}
                                name="password2"
                                type="password"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).password2}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).password2
                                    ? (errors as any).password2
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label='Manager Login'
                                name="login"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).login}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).login
                                    ? (errors as any).login
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("FirstName")}
                                name="firstName"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).firstName}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).firstName
                                    ? (errors as any).firstName
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("LastName")}
                                name="lastName"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).lastName}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).lastName
                                    ? (errors as any).lastName
                                    : ""}
                            </SmallBoldTypography>
                        </Stack>
                    </Grid>
                    <Grid container item xs={6} direction="column" >
                        <Stack spacing={2}>
                            <TextField
                                required
                                label={t("PhoneNumber")}
                                name="phone"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).phone}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).phone
                                    ? (errors as any).phone
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label='Site Url'
                                name="siteUrl"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).siteUrl}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).siteUrl
                                    ? (errors as any).siteUrl
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("Country")}
                                name="state"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).state}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).state
                                    ? (errors as any).state
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("City")}
                                name="city"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).city}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).city
                                    ? (errors as any).city
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("ZipCode")}
                                name="zipCode"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).zipCode}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).zipCode
                                    ? (errors as any).zipCode
                                    : ""}
                            </SmallBoldTypography>

                            <TextField
                                required
                                label={t("Street")}
                                name="street"
                                variant="outlined"
                                onBlur={form.handleBlurEvent}
                                onChange={form.handleChangeEvent}
                                value={(fields as any).street}
                            />
                            <SmallBoldTypography
                                className="error"
                            >
                                {(errors as any).street
                                    ? (errors as any).street
                                    : ""}
                            </SmallBoldTypography>
                        </Stack>
                    </Grid>

                    <Box
                        sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            width: "100%"
                        }}
                    >
                        <Stack spacing={2}>
                            <Button type="submit" variant="contained" value="Submit">
                                {t('CreateAccount')}
                            </Button>

                            <Typography>
                                {t('LoginLabel')} <Link href="/login">{t('LoginAction')}</Link>
                            </Typography>
                        </Stack>
                    </Box>
                </Grid>
            </Stack>
        </Box>
    );
}