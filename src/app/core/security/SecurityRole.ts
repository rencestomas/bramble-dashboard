
export enum SecurityRole {
    Admin = "072b5a84-b969-456f-92c0-89c80d29b7ac",
    Manager = "3d15eef7-38c1-462e-9148-624feac1c14f",
    Kitchen = "149818d1-1759-4dc3-b2ae-22448b0163c1",
    Potman = "820865d6-e005-4860-91ef-1922fe43c49e",
    Guest = "0e82dd1c-9fc8-4fd3-a922-a835650fa09b"
}