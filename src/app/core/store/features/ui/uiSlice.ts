import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'


export interface UiState {
    language: string;
}

const initialState: UiState = {
    language: "sk"
}

export const uiSlice = createSlice({
    name: 'ui',
    initialState,
    reducers: {
        setLanguage: (state, action: PayloadAction<string>) => {
            state.language = action.payload;
        }
    },
})

// Action creators are generated for each case reducer function
export const { setLanguage } = uiSlice.actions

export default { uiReducer: uiSlice.reducer } 
