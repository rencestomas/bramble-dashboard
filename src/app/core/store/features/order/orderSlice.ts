import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { Order } from '../../../../shared/interfaces/order.interface';


export const orderSlice = createSlice({
    name: 'order',
    initialState: {
        orders: [] as Order[]
    },
    reducers: {
        addOrders: (state, action: PayloadAction<Order[]>) => {
            if (state.orders.length > 0) {
                let newOrders = action.payload.filter(c => state.orders.map(i => i.id).indexOf(c.id) === -1);
                if (newOrders && newOrders.length > 0) {
                    state.orders = [...state.orders, ...newOrders];
                }
            } else {
                state.orders = action.payload;
            }
        },
        addOrder: (state, action: PayloadAction<Order>) => {
            state.orders.push(action.payload);
        },
        updateOrder: (state, action: PayloadAction<Order>) => {
            const order = state.orders.find(c => c.id === action.payload.id) as Order;
            Object.assign(order, action.payload);
        },
        updateOrders: (state, action: PayloadAction<Partial<Order>[]>) => {
            for (let change of action.payload) {
                const order = state.orders.find(c => c.id === change.id) as Order;
                Object.assign(order, change);
            }
        },
        upsertOrder: (state, action: PayloadAction<Order>) => {
            const order = state.orders.find(c => c.id === action.payload.id) as Order;
            if (order != null) {
                Object.assign(order, action.payload);
            } else {
                state.orders.push(action.payload);
            }
        },
        upsertOrders: (state, action: PayloadAction<Partial<Order>[]>) => {
            for (let change of action.payload) {
                const order = state.orders.find(c => c.id === change.id) as Order;
                if (order != null) {
                    Object.assign(order, change);
                } else {
                    state.orders.push(change as Order);
                }
            }
        },
        deleteOrder: (state, action: PayloadAction<string>) => {
            state.orders = [...state.orders.filter(c => c.id !== action.payload)];
        },
        deleteOrders: (state, action: PayloadAction<string[]>) => {
            state.orders = [...state.orders.filter(c => action.payload.indexOf(c.id) === -1)];
        }
    },
})

// Action creators are generated for each case reducer function
export const {
    addOrders,
    addOrder,
    upsertOrder,
    upsertOrders,
    updateOrder,
    updateOrders,
    deleteOrder,
    deleteOrders,
} = orderSlice.actions

export default { orderReducer: orderSlice.reducer }
