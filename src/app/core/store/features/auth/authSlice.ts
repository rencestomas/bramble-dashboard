import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { User } from '../../../../shared/interfaces/user.interfaces';
import { Restauration } from '../../../../shared/interfaces/restauration.interface';


export interface AuthState {
    accessToken?: string | null;
    refreshToken?: string | null;
    user?: User | null;
    restauration?: Restauration;
}

const initialState: AuthState = {
    accessToken: null,
    refreshToken: null,
    user: null,
    restauration: undefined
}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        updateAuth: (state, action: PayloadAction<Partial<AuthState>>) => {
            //state.accessToken = action.payload.accessToken;
            //state.refreshToken = action.payload.refreshToken;
            state.user = action.payload.user;
            state.restauration = action.payload.restauration;
        },
        updateToken: (state, action: PayloadAction<Partial<AuthState>>) => {
            //state.accessToken = action.payload.accessToken;
            //state.refreshToken = action.payload.refreshToken;
        },
        updateUserRestauration: (state, action: PayloadAction<Partial<AuthState>>) => {
            state.restauration = action.payload.restauration;
        },
        updateStripeOnboarded: (state, action: PayloadAction<boolean>) => {
            if (state.user) {
                state.user.stripeOnboarded = action.payload;
            }
        }
    },
})

// Action creators are generated for each case reducer function
export const { updateAuth, updateToken, updateUserRestauration, updateStripeOnboarded } = authSlice.actions

export default { authReducer: authSlice.reducer }
