import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { Restauration } from '../../../../shared/interfaces/restauration.interface';


export const restaurantSlice = createSlice({
    name: 'restaurant',
    initialState: {
        restaurants: [] as Restauration[]
    },
    reducers: {
        addRestaurants: (state, action: PayloadAction<Restauration[]>) => {
            if (state.restaurants.length > 0) {
                let newRestaurants = action.payload.filter(c => state.restaurants.map(i => i.id).indexOf(c.id) === -1);
                if (newRestaurants && newRestaurants.length > 0) {
                    state.restaurants = [...state.restaurants, ...newRestaurants];
                }
            } else {
                state.restaurants = action.payload;
            }
        },
        addRestaurant: (state, action: PayloadAction<Restauration>) => {
            state.restaurants.push(action.payload);
        },
        updateRestaurant: (state, action: PayloadAction<Restauration>) => {
            const category = state.restaurants.find(c => c.id === action.payload.id) as Restauration;
            Object.assign(category, action.payload);
        },
        updateRestaurants: (state, action: PayloadAction<Partial<Restauration>[]>) => {
            for (let change of action.payload) {
                const category = state.restaurants.find(c => c.id === change.id) as Restauration;
                Object.assign(category, change);
            }
        },
        deleteRestaurant: (state, action: PayloadAction<string>) => {
            state.restaurants = [...state.restaurants.filter(c => c.id !== action.payload)];
        },
        deleteRestaurants: (state, action: PayloadAction<string[]>) => {
            state.restaurants = [...state.restaurants.filter(c => action.payload.indexOf(c.id) === -1)];
        }
    },
})

// Action creators are generated for each case reducer function
export const {
    addRestaurants,
    addRestaurant,
    updateRestaurant,
    updateRestaurants,
    deleteRestaurant,
    deleteRestaurants,
} = restaurantSlice.actions

export default { restaurantReducer: restaurantSlice.reducer }
