interface Dictionary<T> {
    [Key: string]: T;
}

export const ExceptionMap: Dictionary<string> = {
    "USER__NOT_FOUND": "Účet s týmto menom neexistuje.",
    "USER__WRONG_CREDENTIALS": "Nesprávne meno alebo heslo.",
    "USER__DUPLICATE_EMAIL_OR_NICKNAME": "Profil s týmto menom už existuje.",
    "USER__PASSWORD_DOES_NOT_MATCH": "Heslá sa nezhodujú."
}