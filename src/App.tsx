import './App.css';
import { createTheme, responsiveFontSizes, ThemeProvider } from '@mui/material';
import Layout from './app/components/Layout/Layout';
import { Navigate, Route, Routes } from 'react-router-dom';
import Overview from './pages/Overview/Overview';
import History from './pages/History/History';
import Sales from './pages/Sales/Sales';
import Withdraw from './pages/Withdraw/Withdraw';
import Help from './pages/Help/Help';
import Manage from './pages/Manage/Manage';
import Orders from './pages/Orders/Orders';
import Login from './app/components/Login/Login';
import RequireAuth from './app/core/security/RequireAuth/RequireAuth';
import Unauthorized from './pages/Unauthorized/Unauthorized';
import { SecurityRole } from './app/core/security/SecurityRole';
import Register from './app/components/Register/Register';
import Notification from './app/components/Notification/Notification';
import useNotification from './hooks/useNotification';
import Loading from './app/components/Loading/Loading';
import NotFound from './pages/NotFound/NotFound';
import RouterProvider from './app/core/providers/RouterProvider';
import DailyMenu from './pages/DailyMenu/DailyMenu';
import StandingOffer from './pages/StandingOffer/StandingOffer';
import Modifications from './pages/Modifications/Modifications';
import { setTranslations, setDefaultLanguage, setLanguage } from 'react-multi-lang'
import cz from './translations/cz.json'
import de from './translations/de.json'
import us from './translations/en.json'
import hu from './translations/hu.json'
import pl from './translations/pl.json'
import sk from './translations/sk.json'

import { useSelector } from 'react-redux';
import { RootState } from './app/core/store/store';
import { useEffect } from 'react';
import axios from './app/core/api/axios';
import Admin from './pages/Admin/Admin';
import { SocketProvider } from './app/context/websocketContext';
import FestivalRegister from './app/components/FestivalRegister/FestivalRegister';
import RestaurantRegister from './app/components/RestaurantRegister/RestaurantRegister';
import StripeConnect from './pages/StripeConnect/StripeConnect';


declare module '@mui/material/IconButton' {
  interface IconButtonPropsColorOverrides {
    natural: true;
    black: true;
  }
}

declare module '@mui/material/Button' {
  interface ButtonPropsColorOverrides {
    natural: true;
    black: true;
  }
}

declare module "@mui/material/styles" {
  interface Palette {
    border: {
      main: string,
      secondary: string
    };
  }
  interface PaletteOptions {
    border: {
      main: string,
      secondary: string
    };
  }
  interface Palette {
    backgroundColor: {
      main: string,
      secondary: string
    };
  }
  interface PaletteOptions {
    backgroundColor: {
      main: string,
      secondary: string
    };
  }
  interface Palette {
    natural: {
      main: string
    };
  }
  interface PaletteOptions {
    natural: {
      main: string,
      contrastText: string
    };
    black: {
      main: string,
      contrastText: string
    };
  }
}


let brambleTheme = createTheme({
  components: {
    MuiButton: {
      defaultProps: {
        disableElevation: true,
      },
      styleOverrides: {
        sizeSmall: {
          textTransform: 'none',
          padding: '8px 10px',
          fontWeight: '600',
          borderRadius: '5000px'
        },
        sizeMedium: {
          textTransform: 'none',
          padding: '11px 20px',
          borderRadius: '5000px',
          fontWeight: '600',
          borderWidth: '2px !important'
        },
        sizeLarge: {
          textTransform: 'none',
          padding: '14px 24px',
          fontWeight: '600',
          borderRadius: '5000px'
        }
      }
    },
    MuiCssBaseline: {
      styleOverrides: {
        body: {
          scrollbarColor: "#6b6b6b #2b2b2b",
          "&::-webkit-scrollbar, & *::-webkit-scrollbar": {
            backgroundColor: "white",
            width: "10px"
          },
          "&::-webkit-scrollbar-thumb, & *::-webkit-scrollbar-thumb": {
            borderRadius: 8,
            backgroundColor: "rgba(149,149,149,0.5)",
            minHeight: 24
          },
          "&::-webkit-scrollbar-thumb:focus, & *::-webkit-scrollbar-thumb:focus": {
            backgroundColor: "#959595",
          },
          "&::-webkit-scrollbar-thumb:active, & *::-webkit-scrollbar-thumb:active": {
            backgroundColor: "#959595",
          },
          "&::-webkit-scrollbar-thumb:hover, & *::-webkit-scrollbar-thumb:hover": {
            backgroundColor: "#959595",
          },
          // "&::-webkit-scrollbar-corner, & *::-webkit-scrollbar-corner": {
          //   backgroundColor: "#2b2b2b",
          // },
        },
      },
    },
  },
  typography: {
    fontFamily: [
      'Poppins', 'Roboto', 'Arial',
    ].join(','),
  },
  palette: {
    // mode: 'dark',
    primary: {
      main: '#D81F27',
      contrastText: '#fff'
    },
    secondary: {
      main: '#83BF9F',
      contrastText: '#fff'
    },
    warning: {
      main: '#FF9050',
      contrastText: '#fff'
    },
    natural: {
      main: '#615E83',
      contrastText: '#fff'
    },
    black: {
      main: '#000000',
      contrastText: '#fff'
    },
    backgroundColor: {
      main: '#FAFAFA',
      secondary: '#F8F8FF'
    },
    border: {
      main: '#D81F27',
      secondary: '#E6E6E6'
    },
  }
});

brambleTheme = responsiveFontSizes(brambleTheme);

// Do this two lines only when setting up the application
setTranslations({
  cz,
  de,
  us,
  hu,
  pl,
  sk
})

setDefaultLanguage('us');

export default function App() {

  const { message, severity, autoHideDuration, messageId } = useNotification();

  const language = useSelector((state: RootState) => state.ui.language);

  // set default language based on current browser location on app init 
  useEffect(() => {
    getGeoInfo();
  }, []);

  // set global language on all store changes
  useEffect(() => {
    setLanguage(language?.toLocaleLowerCase());
  });

  async function getGeoInfo() {
    await axios.get('https://ipapi.co/json/').then((response) => {
      let data = response.data;
      setDefaultLanguage(data?.country_code?.toLocaleLowerCase() || 'us');
    }).catch((error) => {
      console.log(error);
    });
  };

  return (
    <>
      <RouterProvider>
        <SocketProvider>
          <ThemeProvider theme={brambleTheme}>
            <Loading />
            <Layout>
              <Routes>
                <Route
                  path="/"
                  element={<Navigate to="/login" />}
                />
                <Route path="*" element={<NotFound />} />

                <Route path='/login' element={<Login />} />
                <Route path='/register' element={<Register />} />
                <Route path='/festival/register' element={<FestivalRegister />} />
                <Route path='/restaurant/register' element={<RestaurantRegister />} />
                <Route path='/unauthorized' element={<Unauthorized />} />

                <Route>
                  <Route element={<RequireAuth allowedRoles={[SecurityRole.Manager, SecurityRole.Kitchen, SecurityRole.Potman]} />}>
                    <Route path='/overview' element={<Overview />} />
                  </Route>

                  <Route element={<RequireAuth allowedRoles={[SecurityRole.Manager, SecurityRole.Kitchen, SecurityRole.Potman]} />}>
                    <Route path='/overview/orders' element={<Orders />} />
                  </Route>

                  <Route element={<RequireAuth allowedRoles={[SecurityRole.Manager, SecurityRole.Kitchen, SecurityRole.Potman]} />}>
                    <Route path='/history' element={<History />} />
                  </Route>

                  {/* <Route element={<RequireAuth allowedRoles={[SecurityRole.Admin, SecurityRole.Manager, SecurityRole.Kitchen, SecurityRole.Potman]} />}>
                  <Route path='/daily-menu' element={<DailyMenu />} />
                </Route> */}

                  <Route element={<RequireAuth allowedRoles={[SecurityRole.Manager, SecurityRole.Kitchen, SecurityRole.Potman]} />}>
                    <Route path='/standing-offer' element={<StandingOffer />} />
                  </Route>

                  <Route element={<RequireAuth allowedRoles={[SecurityRole.Manager, SecurityRole.Kitchen, SecurityRole.Potman]} />}>
                    <Route path='/modifications' element={<Modifications />} />
                  </Route>

                  {/* <Route path='/menu-settings' element={<MenuSettings />} /> */}
                  {/* <Route element={<RequireAuth allowedRoles={[SecurityRole.Admin, SecurityRole.Manager, SecurityRole.Kitchen, SecurityRole.Potman]} />}>
                  <Route path='/sales' element={<Sales />} />
                </Route> */}

                  <Route element={<RequireAuth allowedRoles={[SecurityRole.Manager]} />}>
                    <Route path='/withdraw' element={<Withdraw />} />
                  </Route>

                  <Route element={<RequireAuth allowedRoles={[SecurityRole.Manager]} />}>
                    <Route path='/manage' element={<Manage />} />
                  </Route>

                  <Route element={<RequireAuth allowedRoles={[SecurityRole.Manager]} />}>
                    <Route path='/stripe-connect' element={<StripeConnect />} />
                  </Route>

                  <Route element={<RequireAuth allowedRoles={[SecurityRole.Admin]} />}>
                    <Route path='/admin' element={<Admin />} />
                  </Route>

                  <Route element={<RequireAuth allowedRoles={[SecurityRole.Manager, SecurityRole.Kitchen, SecurityRole.Potman]} />}>
                    <Route path='/help' element={<Help />} />
                  </Route>
                </Route>


                {/* <Route path='/€' element={<History />} /> */}
                {/* <Route path='/any' element={<Manage />} /> */}
              </Routes>
            </Layout>
            <Notification key={messageId} message={message} severity={severity} autoHideDuration={autoHideDuration!} />
          </ThemeProvider>
        </SocketProvider>
      </RouterProvider>
    </>
  )
}